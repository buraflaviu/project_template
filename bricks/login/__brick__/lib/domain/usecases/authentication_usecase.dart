import 'package:dartz/dartz.dart';
import 'package:injectable/injectable.dart';

import '../../core/errors/failures.dart';
import '../../core/utils/extensions.dart';
import '../repositories_contracts/authentication_repo.dart';
import '../validators/authentication_validator.dart';

@injectable
class AuthenticationUseCase {
  AuthenticationRepo authRepo;

  AuthenticationValidator fieldValidator;

  AuthenticationUseCase({
    required this.authRepo,
    required this.fieldValidator,
  });

  Future<Either<Failure, String>> loginClassic(
    String? email,
    String? password,
    bool areTermsAccepted,
    bool isRememberMeAccepted,
  ) async {
    return fieldValidator
        .areLoginInformationValid(email, password, areTermsAccepted)
        .fold(
          left,
          (success) => authRepo.loginClassic(
            email!,
            password!,
            isRememberMeAccepted,
          ),
        );
  }

  Future<Either<Failure, String>> loginWithMicrosoft(
    bool areTermsAccepted,
  ) async {
    return fieldValidator.checkTermsCondition(areTermsAccepted).fold(
          left,
          (success) => authRepo.loginWithMicrosoft(),
        );
  }

  Future<Either<Failure, String>> changePassword(
    String? currentPassword,
    String? newPassword,
    String? confirmPassword,
  ) async {
    return fieldValidator
        .isNewPasswordValid(currentPassword, newPassword, confirmPassword)
        .fold(
          left,
          (success) => authRepo.changePassword(
            currentPassword!,
            newPassword!,
          ),
        );
  }
}
