import 'dart:async';

import 'package:dio/dio.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart' as storage;

import '../../../../core/utils/constants.dart';
import '../../../../core/utils/extensions.dart';
import '../../../../core/utils/logger.dart';

class RequestHandlerInterceptor extends QueuedInterceptor {
  String accessToken = AppConst.EMPTY_STRING;

  @override
  Future<void> onRequest(
    RequestOptions options,
    RequestInterceptorHandler handler,
  ) async {
    accessToken =
        await storage.FlutterSecureStorage().read(key: AppKey.ACCESS_TOKEN) ??
            AppConst.EMPTY_STRING;
    options.headers['Authorization'] = 'Bearer $accessToken';
    options.headers['Content-Type'] = 'application/json';
    options.headers['Accept'] = '*/*';
    Logger.success(runtimeType, AppString.accessTokenAdded);
    super.onRequest(options, handler);
  }

  @override
  Future onError(DioError err, ErrorInterceptorHandler handler) async {
    if (err.response?.statusCode != null) {
      handler.resolve(err.response!);
      return;
    }
    super.onError(err, handler);
  }

  Future revokeAccess() async {
    await storage.FlutterSecureStorage().delete(key: AppKey.ACCESS_TOKEN);
    await storage.FlutterSecureStorage().delete(key: AppKey.REFRESH_TOKEN);
    accessToken = AppConst.EMPTY_STRING;
  }
}
