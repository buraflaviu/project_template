import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';

import '../../../../core/utils/helpers.dart';

class LoginCredentialsOptions extends StatefulWidget {
  final Function onRememberStateChanged;

  final Function onForgotPasswordTap;

  final bool isRememberChecked;

  const LoginCredentialsOptions({
    Key? key,
    this.isRememberChecked = false,
    required this.onRememberStateChanged,
    required this.onForgotPasswordTap,
  }) : super(key: key);

  @override
  State<LoginCredentialsOptions> createState() =>
      _LoginCredentialsOptionsState();
}

class _LoginCredentialsOptionsState extends State<LoginCredentialsOptions> {
  late bool isRememberChecked;

  @override
  void initState() {
    isRememberChecked = widget.isRememberChecked;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Padding(
          padding: EdgeInsets.only(bottom: 1),
          child: Checkbox(
    
    activeColor: Theme.of(context).colorScheme.primary,
            splashRadius: 10,
            value: isRememberChecked,
            onChanged: (value) {
              setState(() {
                widget.onRememberStateChanged(value);
                isRememberChecked = !isRememberChecked;
              });
            },
          ),
        ),
        Expanded(
          child: Container(
            padding: EdgeInsets.only(bottom: 1),
            child: Text(
              'Remember me',
              style: TextStyle(color: hexToColor('#A0AABD'), fontSize: 10.sp),
            ),
          ),
        ),
        InkWell(
          onTap: () => widget.onForgotPasswordTap,
          child: Container(
            padding: EdgeInsets.only(bottom: 1),
            child: Text(
              'Forgot password?',
              style: TextStyle(
    
    color: Theme.of(context).colorScheme.primary,
                fontSize: 10.sp,
                fontWeight: FontWeight.w600,
              ),
            ),
          ),
        )
      ],
    );
  }
}
