part of 'authentication_bloc.dart';

@immutable
abstract class AuthenticationEvent {}

@immutable
class LoginFetchDataEvent extends AuthenticationEvent {}

@immutable
class ClassicLoginEvent extends AuthenticationEvent {
  final String? email;
  final String? password;

  final bool areTermsAccepted;
  final bool isRememberMeAccepted;

  ClassicLoginEvent({
    required this.email,
    required this.password,
    required this.areTermsAccepted,
    required this.isRememberMeAccepted,
  });
}

class ChangePasswordEvent extends AuthenticationEvent {
  final String? currentPassword;
  final String? newPassword;
  final String? confirmPassword;

  ChangePasswordEvent({
    required this.currentPassword,
    required this.newPassword,
    required this.confirmPassword,
  });
}

@immutable
class SignInWithMicrosoftEvent extends AuthenticationEvent {
  final bool areTermsAccepted;

  SignInWithMicrosoftEvent({required this.areTermsAccepted});
}
