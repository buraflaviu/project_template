import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:injectable/injectable.dart';

import '../../../../core/errors/exceptions.dart';
import '../../../../core/utils/constants.dart';
import '../../../../core/utils/extensions.dart';
import '../../../models/localisation/language_model.dart';
import '../../../models/localisation/localisation_model.dart';
import '../components/custom_dio.dart';

@singleton
class LocalisationRemoteApi {
  CustomDio dio = CustomDio();

  Future<LocalisationModel> getLanguageForLogin(int id) async {
    await dotenv.load();
     await FlutterSecureStorage().read(key: AppKey.APP_SCOPE_ID);
    final String notificationTypesUrl =
        "${dotenv.env[AppConst.LOCALIZATION_URL] ?? ''}languagetranslation/get-public/$id/1";
    final response = await dio.makeSecureCall(
      () => dio.instance.getUri(
        Uri.parse(notificationTypesUrl),
      ),
    );

    return response.fold(
      (fail) => throw HardException(runtimeType, fail.failureMessage),
      (response) async {
        if (response.statusCode == 200) {
          final model = LocalisationModel.fromJson(
            response.data as Map<String, dynamic>,
          );
          return model;
        } else {
          throw MediumException(
            runtimeType,
            AppString.wrongStatusCode.translate(),
          );
        }
      },
    );
  }

  Future<LocalisationModel> getLanguageAfterLogin(int id) async {
    await dotenv.load();
     await FlutterSecureStorage().read(key: AppKey.APP_SCOPE_ID);
    final String notificationTypesUrl =
        "${dotenv.env[AppConst.LOCALIZATION_URL] ?? ''}languagetranslation/$id/1";
    final response = await dio.makeSecureCall(
      () => dio.instance.getUri(
        Uri.parse(notificationTypesUrl),
      ),
    );

    return response.fold(
      (fail) => throw HardException(runtimeType, fail.failureMessage),
      (response) async {
        if (response.statusCode == 200) {
          final model = LocalisationModel.fromJson(
            response.data as Map<String, dynamic>,
          );
          return model;
        } else {
          throw MediumException(
            runtimeType,
            AppString.wrongStatusCode.translate(),
          );
        }
      },
    );
  }

  Future<List<LanguageModel>?> getAllAvailableLanguages() async {
    await dotenv.load();
    final String notificationTypesUrl =
        "${dotenv.env[AppConst.LOCALIZATION_URL] ?? ''}language/iso-codes";
    final response = await dio.makeSecureCall(
      () => dio.instance.getUri(
        Uri.parse(notificationTypesUrl),
      ),
    );

    return response.fold(
      (fail) => throw HardException(runtimeType, fail.failureMessage),
      (response) {
        if (response.statusCode == 200) {
          final responseData = response.data as List<dynamic>;
          if (responseData.isNotEmpty) {
            final listOfModel = responseData
                .map<LanguageModel>(
                  (json) => LanguageModel.fromJson(
                    json as Map<String, dynamic>,
                  ),
                )
                .toList();

            return listOfModel;
          } else {
            return [];
          }
        } else {
          throw MediumException(
            runtimeType,
            AppString.wrongStatusCode.translate(),
          );
        }
      },
    );
  }
}
