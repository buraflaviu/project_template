import 'package:dartz/dartz.dart';
{{#localisation_locale_available}}import 'package:flutter/services.dart';
import 'dart:convert';
  {{/localisation_locale_available}}
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:injectable/injectable.dart';

import '../../core/errors/exceptions.dart';
import '../../core/errors/failures.dart';
import '../../core/utils/constants.dart';
import '../../core/utils/logger.dart';
import '../../core/utils/translator.dart';
import '../../domain/repositories_contracts/localisation_repo.dart';
import '../datasource/remote/api/localisation_remote_api.dart';
import '../models/localisation/language_model.dart';

@Singleton(as: LocalisationRepository)
class LocalisationRepoImpl extends LocalisationRepository {
  LocalisationRemoteApi localisationRemoteApi;

  Translator translator;

  List<LanguageModel> listOfAvailableLanguages = [];

  LocalisationRepoImpl({
    required this.localisationRemoteApi,
    required this.translator,
  });

  @override
  Future<Either<Failure, List<String>>> fetchAllLanguages() async {
    if (listOfAvailableLanguages.isNotEmpty) {
      return Right(listOfAvailableLanguages.map((e) => e.name).toList());
    }
    try {
      final List<LanguageModel>? listOfLanguages =
          await localisationRemoteApi.getAllAvailableLanguages();
      listOfLanguages?.forEach((element) async {
        listOfAvailableLanguages.add(element);
      });

      translator.availableLanguagesNames =
          listOfAvailableLanguages.map((e) => e.name).toList();
      return Right(translator.availableLanguagesNames);
    } on AppException catch (appException) {
      return Left(HardFailure(failureMessage: appException.message));
    } catch (exception) {
      Logger.error(runtimeType, exception.toString());
      return Left(ServerFailure(failureMessage: exception.toString()));
    }
  }

  Future<Either<Failure, Map<String, String>>> getLanguageForLogin(
    int id,
  ) async {
    try {
      final model = await localisationRemoteApi.getLanguageForLogin(id);
      final translationData = model.getTranslation();
      return Right(translationData);
    } on AppException catch (appException) {
      return Left(HardFailure(failureMessage: appException.message));
    } catch (exception) {
      Logger.error(runtimeType, exception.toString());
      return Left(ServerFailure(failureMessage: exception.toString()));
    }
  }

  Future<Either<Failure, Map<String, String>>> getLanguageAfterLogin(
    int id,
  ) async {
    try {
      final model = await localisationRemoteApi.getLanguageAfterLogin(id);
      final translationData = model.getTranslation();
      return Right(translationData);
    } on AppException catch (appException) {
      return Left(HardFailure(failureMessage: appException.message));
    } catch (exception) {
      Logger.error(runtimeType, exception.toString());
      return Left(ServerFailure(failureMessage: exception.toString()));
    }
  }

  @override
  Future changeCurrentLanguage(String? languageName) async {
    final newLanguage = listOfAvailableLanguages.firstWhere(
      (element) => element.name == languageName,
      orElse: () => translator.defaultLanguage,
    );
    if (translator.currentLanguage.isoCode != newLanguage.isoCode) {
      Map<String, String> finalTranslation = {};
      final language = listOfAvailableLanguages.firstWhere(
        (element) =>
            element.isoCode.toLowerCase() == newLanguage.isoCode.toLowerCase(),
      );
      if (translator.availableTranslationsAfterLogin[language.id] == null) {
        (await getLanguageAfterLogin(language.id)).fold((l) {
          return;
        }, (translation) {
          finalTranslation = translation;
        });
      } else {
        finalTranslation =
            translator.availableTranslationsAfterLogin[language.id]!;
      }

      translator
        ..currentLanguage = language
        ..setCurrentTranslationAfterLogin(language.id, finalTranslation);
      await FlutterSecureStorage()
          .write(key: AppKey.CURRENT_LANGUAGE_ID, value: '${language.id}');
    }
  }

  @override
  Future loadLanguageForLogin() async {
    Either<Failure, Map<String, String>> translationResponse;
    int languageId;
    final storedLanguage =
        await FlutterSecureStorage().read(key: AppKey.CURRENT_LANGUAGE_ID);

    if (storedLanguage != null) {
      languageId = int.parse(storedLanguage);
    } else {
      languageId = translator.defaultLanguage.id;
    }

    if (translator.currentLanguage.id == languageId) {
      if (translator.currentTranslation.isEmpty) {
        {{#localisation_locale_available}}
        final translation = jsonDecode(
          await rootBundle.loadString('assets/translations/$languageId.txt'),
        ) as Map<String, dynamic>;
        translationResponse = Right(Map<String, String>.from(translation));
        {{/localisation_locale_available}}
        {{^localisation_locale_available}}
        translationResponse = await getLanguageForLogin(languageId);
        {{/localisation_locale_available}}
      } else {
        return;
      }
    } else {
    {{#localisation_locale_available}}
    final translation = jsonDecode(
    await rootBundle.loadString('assets/translations/$languageId.txt'),
    ) as Map<String, dynamic>;
    translationResponse = Right(Map<String, String>.from(translation));
    {{/localisation_locale_available}}
    {{^localisation_locale_available}}
    translationResponse = await getLanguageForLogin(languageId);
    {{/localisation_locale_available}}
    }

    await translationResponse.fold((l) {
      return;
    }, (translation) async {
      translator
        ..setCurrentTranslationBeforeLogin(languageId, translation)
        ..currentLanguage = listOfAvailableLanguages
            .firstWhere((element) => element.id == languageId);
      await FlutterSecureStorage()
          .write(key: AppKey.CURRENT_LANGUAGE_ID, value: '$languageId');
    });

    await Future.delayed(Duration(milliseconds: 1500), () {});
  }

  @override
  Future loadTranslationAfterLogin() async {
    final languageId = translator.currentLanguage.id;
    if (translator.availableTranslationsAfterLogin[languageId] == null) {
      (await getLanguageAfterLogin(languageId)).fold((l) {
        return;
      }, (translation) {
        final newTranslation = translator.currentTranslation
          ..addAll(translation);
        translator.setCurrentTranslationAfterLogin(languageId, newTranslation);
      });
    }

    await Future.delayed(Duration(milliseconds: 1500), () {});
  }
}
