part of '{{pageName.snakeCase()}}_bloc.dart';

@immutable
abstract class {{pageName.pascalCase()}}Event {}

class {{pageName.pascalCase()}}FetchDataEvent extends {{pageName.pascalCase()}}Event {}

class {{pageName.pascalCase()}}CompleteTaskEvent extends {{pageName.pascalCase()}}Event {
  final bool completeTask;

{{pageName.pascalCase()}}CompleteTaskEvent({required this.completeTask});
}
