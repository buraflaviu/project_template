import '../../../domain/entities/user_info_entity.dart';

class UserInfoModel extends UserInfoEntity {
  final int id;

  UserInfoModel({
    required this.id,
    required super.userId,
    required super.description,
    required super.isTaskCompleted,
  });

  factory UserInfoModel.fromJson(Map<String, dynamic> json) => UserInfoModel(
      id: json['id'] as int,
      userId: json['userId'] as int,
      description: json['title'] as String,
      isTaskCompleted: json['completed'] as bool,);

  Map<String, dynamic> toJson() => {
    'id': id,
    'userId': userId,
    'title': description,
    'completed': isTaskCompleted,
  };
}
