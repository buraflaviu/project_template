import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';

import '../../../../domain/repositories_contracts/{{pageName.snakeCase()}}_repo.dart';

part '{{pageName.snakeCase()}}_event.dart';

part '{{pageName.snakeCase()}}_state.dart';

class {{pageName.pascalCase()}}Bloc extends Bloc<{{pageName.pascalCase()}}Event, {{pageName.pascalCase()}}State> {
{{pageName.pascalCase()}}Repository {{pageName.camelCase()}}Repository;

{{pageName.pascalCase()}}Bloc({required this.{{pageName.camelCase()}}Repository}) : super({{pageName.pascalCase()}}InitialState()) {
    on<{{pageName.pascalCase()}}FetchDataEvent>(_fetchDataEvent);
    on<{{pageName.pascalCase()}}CompleteTaskEvent>(_completeEvent);
  }

  _fetchDataEvent(event, emit) async {
    emit({{pageName.pascalCase()}}LoadingState());
    final userInfoResponse = await {{pageName.camelCase()}}Repository.getUserInfo();
    userInfoResponse.fold((failure) {
      emit({{pageName.pascalCase()}}DataFetchedFailedState(message: failure.failureMessage));
    }, (userInfo) {
      emit(
{{pageName.pascalCase()}}DataFetchedState(
          userId: userInfo.userId,
          taskDescription: userInfo.description,
          isStatusCompleted: userInfo.isTaskCompleted,
        ),
      );
    });
  }

  _completeEvent({{pageName.pascalCase()}}CompleteTaskEvent event, emit) async {
    emit({{pageName.pascalCase()}}CompleteTaskLoadingState());
    final taskResponse =
        await {{pageName.camelCase()}}Repository.completeTask(event.completeTask);
    taskResponse.fold((failure) {
      emit({{pageName.pascalCase()}}CompleteTaskFailedState(message: failure.failureMessage));
    }, (userInfo) {
      emit({{pageName.pascalCase()}}CompleteTaskSuccessState());
    });
  }
}
