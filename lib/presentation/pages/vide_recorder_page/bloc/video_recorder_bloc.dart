import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';

import '../../../../domain/repositories_contracts/video_recorder_repo.dart';

part 'video_recorder_event.dart';
part 'video_recorder_state.dart';

class VideRecorderBloc extends Bloc<VideRecorderEvent, VideRecorderState> {
  VideoRecorderRepository videoRecorderRepository;

  VideRecorderBloc({required this.videoRecorderRepository})
      : super(VideRecorderInitialState()) {
    on<VideRecorderUploadVideoEvent>(_fetchDataEvent);
  }

  _fetchDataEvent(VideRecorderUploadVideoEvent event, emit) async {
    final userInfoResponse =
        await videoRecorderRepository.uploadVideo(event.path, event.name);
    userInfoResponse.fold((failure) {
      emit(VideRecorderUploadFailedState(message: failure.failureMessage));
    }, (successMessage) {
      emit(
        VideRecorderUploadSuccessfullyState(message: successMessage),
      );
    });
  }
}
