import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:sizer/sizer.dart';

import '../../../core/utils/constants.dart';
import '../../../core/utils/helpers.dart';
import '../../../core/utils/extensions.dart';

import '../../bloc/authentication_bloc/authentication_bloc.dart';
import '../../shared_widgets/credentials_field.dart';
import '../../shared_widgets/elevated_action_button.dart';
import 'widgets/login_credentials_options.dart';
import 'widgets/terms_and_privacy_options.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  LoginPageState createState() => LoginPageState();
}

class LoginPageState extends State<LoginPage> {
  String? email;
  String? password;
  bool areTermsAccepted = false;
  bool isRememberMeAccepted = true;

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Theme.of(context).colorScheme.background,
    
      child: SafeArea(
        child: Scaffold(
          resizeToAvoidBottomInset: false,
          body: BlocConsumer<AuthenticationBloc, AuthenticationState>(
            listenWhen: (previousState, state) =>
                state.runtimeType != LoginInitialState,
            listener: (context, state) {
              if (state is LoadingState) {
                EasyLoading.show();
              }
              if (state is LoginFailedState) {
                EasyLoading.showError(state.failMessage);
              }
              if (state is LoginSuccessState) {
                if (EasyLoading.isShow) EasyLoading.dismiss();
                //todo on login success
              }
              if (state is FetchDataFinishedState) {
                if (EasyLoading.isShow) EasyLoading.dismiss();
                email = state.email;
                password = state.password;
              }
            },
            builder: (context, state) {
              return Container(
    
    color: Theme.of(context).colorScheme.background,
                child: Padding(
                  padding: EdgeInsets.symmetric(horizontal: 5.w),
                  child: Column(
                    children: [
                      Center(
                        child: SizedBox(
                          width: double.infinity,
                          height: 8.h,
                          child: Image.asset(
                            AppPaths.shiftingLogo,
                            height: 1.h,
                            fit: BoxFit.fitHeight,
                          ),
                        ),
                      ),
                      SizedBox(height: 4.h),
                      CredentialsField(
                        fieldNameColor: hexToColor(AppColors.lightBlue),
    
    fieldColor: Theme.of(context).colorScheme.secondaryContainer,
                        borderRadius: 20,
                        fieldTitle: AppString.fieldEmailTitle.translate(),
                        initialValue: email,
                        onTextChanged: (String value) {
                          email = value;
                        },
                        hintText: AppString.fieldEmailHint.translate(),
                      ),
                      SizedBox(height: 2.h),
                      CredentialsField(
                        fieldNameColor: hexToColor(AppColors.lightBlue),
    
    fieldColor: Theme.of(context).colorScheme.secondaryContainer,
                        borderRadius: 20,
                        fieldTitle: AppString.fieldPasswordTitle.translate(),
                        initialValue: password,
                        onTextChanged: (String value) {
                          password = value;
                        },
                        hintText: AppString.fieldPasswordHint.translate(),
                        visibilityOption: true,
                      ),
                      SizedBox(height: 2.h),
                      LoginCredentialsOptions(
                        isRememberChecked: isRememberMeAccepted,
                        onRememberStateChanged: (bool state) {
                          isRememberMeAccepted = state;
                        },
                        onForgotPasswordTap: () {},
                      ),
                      SizedBox(height: 6.h),
                      ElevatedActionButton(
                        isEnabled: state is! LoadingState,
                        onTap: () {
                          context.read<AuthenticationBloc>().add(
                                ClassicLoginEvent(
                                  isRememberMeAccepted: isRememberMeAccepted,
                                  areTermsAccepted: areTermsAccepted,
                                  email: email,
                                  password: password,
                                ),
                              );
                        },
                        text: AppString.logIn.translate(),
                        width: 40.w,
                        height: 5.h,
                      ),
                      SizedBox(height: 3.h),
                      ElevatedActionButton(
                        isEnabled: state is! LoadingState,
                        onTap: () {
                          context.read<AuthenticationBloc>().add(
                                SignInWithMicrosoftEvent(
                                  areTermsAccepted: areTermsAccepted,
                                ),
                              );
                        },
                        text: AppString.signWithMicrosoft.translate(),
                        icon: Image.asset(
                          AppPaths.microsoftLogo,
                          height: double.infinity,
                          width: double.infinity,
                          fit: BoxFit.cover,
                        ),
                        height: 5.h,
                        width: 60.w,
                      ),
                      SizedBox(height: 3.h),
                      TermsAndPrivacyOptions(
                        areTermsChecked: areTermsAccepted,
                        onTermsStateChanged: (bool state) {
                          areTermsAccepted = state;
                        },
                      ),
                    ],
                  ),
                ),
              );
            },
          ),
        ),
      ),
    );
  }
}
