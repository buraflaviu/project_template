import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';

class CredentialsField extends StatefulWidget {
  final String fieldTitle;

  final bool visibilityOption;

  final bool leadingIcon;

  final double borderRadius;

  final Color? fieldNameColor;

  final Color? fieldColor;

  final String? hintText;

  final String? initialValue;

  final Function onTextChanged;

  const CredentialsField({
    Key? key,
    required this.fieldTitle,
    required this.onTextChanged,
    required this.borderRadius,
    this.fieldColor,
    this.fieldNameColor,
    this.visibilityOption = false,
    this.leadingIcon = true,
    this.initialValue,
    this.hintText = '',
  }) : super(key: key);

  @override
  State<CredentialsField> createState() => _CredentialsFieldState();
}

class _CredentialsFieldState extends State<CredentialsField> {
  late bool isVisible;
  String? textValue;
  TextEditingController controller = TextEditingController();

  @override
  void initState() {
    isVisible = widget.visibilityOption;
    textValue = widget.initialValue;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    controller.text = textValue ?? widget.initialValue ?? '';
    return GestureDetector(
      onTap: () {
        FocusScope.of(context).requestFocus(FocusNode());
      },
      child: Column(
        children: [
          Row(
            children: [
              Container(
                padding: EdgeInsets.only(bottom: 1.h),
                child: Text(
                  widget.fieldTitle,
                  style: TextStyle(
                    color: widget.fieldNameColor,
                    fontSize: 12.sp,
                  ),
                ),
              )
            ],
          ),
          Row(
            children: [
              Expanded(
                child: Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(widget.borderRadius),
                    color: widget.fieldColor,
                    border: Border.all(
                      color: Colors.transparent,
                    ),
                  ),
                  child: ListTile(
                    title: TextFormField(
    style: TextStyle(color: Theme.of(context).colorScheme.onSecondaryContainer),
                      onChanged: (value) {
                        widget.onTextChanged(value);
                        textValue = value;
                      },
                      decoration: InputDecoration.collapsed(
                        hintStyle: TextStyle(fontSize: 10.sp),
                        hintText: widget.hintText,
                      ),
                      obscureText: isVisible,
                      controller: controller,
                    ),
                    leading: widget.leadingIcon == true
                        ? CircleAvatar(
    
    backgroundColor: Theme.of(context).colorScheme.secondary,
                            radius: 15,
                            child: Icon(
                              Icons.lock,
                              color: Colors.white,
                            ),
                          )
                        : null,
                    trailing: widget.visibilityOption == true
                        ? InkWell(
                            onTap: () {
                              setState(() {
                                if (textValue != null ||
                                    widget.initialValue != null) {
                                  isVisible = !isVisible;
                                }
                              });
                            },
                            child: ClipRRect(
                              borderRadius: BorderRadius.circular(20),
                              child: !isVisible
                                  ? Image.asset(
                                      'assets/images/view.png',
                                      height: 2.4.h,
                                    )
                                  : Image.asset(
                                      'assets/images/hide.png',
                                      height: 2.4.h,
                                    ),
                            ),
                          )
                        : SizedBox(
                            width: 0.1,
                            height: 0.1,
                          ),
                  ),
                ),
              )
            ],
          ),
        ],
      ),
    );
  }
}
