import 'package:bloc/bloc.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:injectable/injectable.dart';
import 'package:meta/meta.dart';

import '../../../core/utils/constants.dart';
import '../../../core/utils/logger.dart';
import '../../../domain/usecases/authentication_usecase.dart';

part 'authentication_event.dart';
part 'authentication_state.dart';

@singleton
class AuthenticationBloc
    extends Bloc<AuthenticationEvent, AuthenticationState> {
  final AuthenticationUseCase authenticationUseCase;

  AuthenticationBloc({
    required this.authenticationUseCase,
  }) : super(LoginInitialState()) {
    on<LoginFetchDataEvent>(_fetchDataForLogin);
    on<ClassicLoginEvent>(_initiateClassicLogin);
    on<SignInWithMicrosoftEvent>(_initiateSignInWithMicrosoft);
    on<ChangePasswordEvent>(_initiateChangePassword);
  }

  _fetchDataForLogin(event, emit) async {
    emit(LoadingState());
    String? email, password;
    email = await FlutterSecureStorage().read(key: AppKey.USER_EMAIL);
    password = await FlutterSecureStorage().read(key: AppKey.USER_PASSWORD);
    emit(FetchDataFinishedState(email: email, password: password));
    Logger.success(
      runtimeType,
      '${state.runtimeType} data: $email - $password',
    );
  }

  _initiateClassicLogin(ClassicLoginEvent event, emit) async {
    emit(LoadingState());
    (await authenticationUseCase.loginClassic(
      event.email,
      event.password,
      event.areTermsAccepted,
      event.isRememberMeAccepted,
    ))
        .fold(
      (failure) => emit(LoginFailedState(failMessage: failure.failureMessage)),
      (status) => emit(LoginSuccessState(statusMessage: status)),
    );
  }

  _initiateSignInWithMicrosoft(SignInWithMicrosoftEvent event, emit) async {
    emit(LoadingState());
    (await authenticationUseCase.loginWithMicrosoft(event.areTermsAccepted))
        .fold(
      (failure) => emit(LoginFailedState(failMessage: failure.failureMessage)),
      (status) => emit(LoginSuccessState(statusMessage: status)),
    );
  }

  _initiateChangePassword(ChangePasswordEvent event, emit) async {
    emit(SavingPasswordState());
    (await authenticationUseCase.changePassword(
      event.currentPassword,
      event.newPassword,
      event.confirmPassword,
    ))
        .fold(
            (failure) => emit(
                  ChangePasswordFailedState(
                    failMessage: failure.failureMessage,
                  ),
                ), (status) {
      emit(ChangePasswordSuccessState(statusMessage: status));
      Logger.success(
        runtimeType,
        '${state.runtimeType} ${AppLogger.passwordSuccessfullySaved}',
      );
    });
  }
}
