class TranslatedWord {
  String? translationCode;
  String? translation;

  TranslatedWord({this.translationCode, this.translation});

  TranslatedWord.fromJson(Map<String, dynamic> json) {
    translationCode = json['translationCode'] as String?;
    translation = json['translation'] as String?;
  }

  Map<String, dynamic> toJson() => {
        'translationCode': translationCode,
        'translation': translation,
      };
}
