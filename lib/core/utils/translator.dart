import 'package:injectable/injectable.dart';

import '../../data/models/localisation/language_model.dart';

@singleton
class Translator {
  Map<int, Map<String, String>> availableTranslationsBeforeLogin = {};
  Map<int, Map<String, String>> availableTranslationsAfterLogin = {};
  List<String> availableLanguagesNames = [];
  Map<String, String> currentTranslation = {};
  late LanguageModel currentLanguage;
  LanguageModel defaultLanguage = LanguageModel(
    id: 1,
    isoCode: 'en',
    name: 'English',
  );

  Translator() {
    currentLanguage = defaultLanguage;
  }

  void setCurrentTranslationBeforeLogin(
    int languageId,
    Map<String, String> translation,
  ) {
    currentTranslation = translation;
    availableTranslationsBeforeLogin[languageId] = currentTranslation;
  }

  void setCurrentTranslationAfterLogin(
    int languageId,
    Map<String, String> translation,
  ) {
    currentTranslation = translation;
    availableTranslationsAfterLogin[languageId] = currentTranslation;
  }

  String translate(String key) => currentTranslation[key] ?? key;
}
