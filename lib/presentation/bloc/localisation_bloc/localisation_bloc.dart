import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:injectable/injectable.dart';

import '../../../domain/repositories_contracts/localisation_repo.dart';

part 'localisation_event.dart';
part 'localisation_state.dart';

@injectable
class LocalisationBloc extends Bloc<LocalisationEvent, LocalisationState> {
  LocalisationRepository localisationRepo;

  LocalisationBloc({required this.localisationRepo})
      : super(LocalisationInitial()) {
    on<LocalisationChangeLanguage>((event, emit) async {
      emit(LocalisationLoading());
      await localisationRepo.changeCurrentLanguage(event.languageName);
      emit(LocalisationLanguageChanged());
    });

    on<LocalisationLanguageForLogin>((event, emit) async {
      emit(LocalisationLoading());
      await localisationRepo.fetchAllLanguages();
      await localisationRepo.loadLanguageForLogin();
      emit(LocalisationBeforeLoginFetched());
    });

    on<LocalisationLanguageAfterLogin>((event, emit) async {
      emit(LocalisationLoading());
      await localisationRepo.loadTranslationAfterLogin();
      emit(LocalisationAfterLoginFetched());
    });
  }
}
