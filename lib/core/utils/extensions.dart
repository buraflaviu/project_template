import 'package:flutter/foundation.dart';

import 'constants.dart';
import 'translator.dart';
import '../di/injectable.dart';

extension StringExtensions on String? {
  String? get ifDebugging => kDebugMode ? this : null;

  bool get isNullOrEmpty {
    if (this == null) return true;
    if (this!.isEmpty) return true;
    return false;
  }

  bool get isEmptyString => this == AppConst.EMPTY_STRING;

  bool get isNotEmptyString => !(this == AppConst.EMPTY_STRING);

  
  String translate() =>
      getIt<Translator>().translate(this ?? AppConst.EMPTY_STRING);
  
  
}


extension DateTimeExtension on DateTime {
  bool isDateEqual(DateTime dateTime) {
    return year == dateTime.year &&
        month == dateTime.month &&
        day == dateTime.day;
  }

  DateTime get lastDayOfMonth =>
      DateTime(year, month + 1).subtract(Duration(days: 1));

  DateTime get firstDayOfMonth => DateTime(year, month);
}
