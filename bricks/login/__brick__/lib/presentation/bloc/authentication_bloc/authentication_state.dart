part of 'authentication_bloc.dart';

@immutable
abstract class AuthenticationState {}

class LoginInitialState extends AuthenticationState {}

@immutable
class LoadingState extends AuthenticationState {}

@immutable
class LoginFailedState extends AuthenticationState {
  final String failMessage;

  LoginFailedState({required this.failMessage});
}

@immutable
class LoginSuccessState extends AuthenticationState {
  final String statusMessage;

  LoginSuccessState({required this.statusMessage});
}

@immutable
class FetchDataFinishedState extends AuthenticationState {
  final String? email;
  final String? password;

  FetchDataFinishedState({this.email, this.password});
}

@immutable
class ChangePasswordFailedState extends AuthenticationState {
  final String failMessage;

  ChangePasswordFailedState({required this.failMessage});
}

@immutable
class ChangePasswordSuccessState extends AuthenticationState {
  final String statusMessage;

  ChangePasswordSuccessState({required this.statusMessage});
}

@immutable
class SavingPasswordState extends AuthenticationState {}
