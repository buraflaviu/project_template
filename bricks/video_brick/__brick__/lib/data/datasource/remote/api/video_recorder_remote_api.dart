import 'package:dio/dio.dart';

class VideoRecorderRemoteApi {
  Dio dio = Dio();
  String token =
      'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJodHRwOi8vc2NoZW1hcy5taWNyb3NvZnQuY29tL3dzLzIwMDgvMDYvaWRlbnRpdHkvY2xhaW1zL3ByaW1hcnlzaWQiOiIxMTYwIiwiaHR0cDovL3NjaGVtYXMubWljcm9zb2Z0LmNvbS93cy8yMDA4LzA2L2lkZW50aXR5L2NsYWltcy9yb2xlIjoiUGFjaWVudCIsImlkVW5pdGF0ZVV0aWxpemF0b3JpIjoiMTIwNyIsImlkVW5pdGF0ZSI6IjEiLCJleHAiOjE2NTY2OTUwMDB9.xV9eJLFP0T_ybHDpUv2kFYKv7HXV3eWP_LEQLerHol8';

  Future<String> uploadVideo(String path, String name) async {
    final dio = Dio();

    final formData = FormData.fromMap({
      'IdCed': 2027,
      'Video': await MultipartFile.fromFile(path, filename: name),
    });

    final response = await dio.postUri(
      Uri.parse(
        'https://dev-motric.azurewebsites.net/api/chestionare-executii/upload-video',
      ),
      data: formData,
      options: Options(
        headers: {
          'Authorization': 'Bearer $token',
          'Content-Type': 'multipart/form-data',
        },
      ),
    );
    return response.statusMessage.toString();
  }
}
