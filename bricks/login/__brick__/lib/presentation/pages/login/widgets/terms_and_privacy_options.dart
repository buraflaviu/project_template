import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';

import '../../../../core/utils/constants.dart';
import '../../../../core/utils/extensions.dart';
import '../../../../core/utils/helpers.dart';
import '../../../shared_widgets/details_alert_dialog.dart';

class TermsAndPrivacyOptions extends StatefulWidget {
  final Function onTermsStateChanged;

  final bool areTermsChecked;

  const TermsAndPrivacyOptions({
    Key? key,
    this.areTermsChecked = false,
    required this.onTermsStateChanged,
  }) : super(key: key);

  @override
  State<TermsAndPrivacyOptions> createState() => _TermsAndPrivacyOptionsState();
}

class _TermsAndPrivacyOptionsState extends State<TermsAndPrivacyOptions> {
  late bool areTermsChecked;

  @override
  void initState() {
    areTermsChecked = widget.areTermsChecked;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Checkbox(
        {{^theme_available}}activeColor: Colors.orange[700],{{/theme_available}}
        {{#theme_available}}activeColor: Theme.of(context).colorScheme.primary,{{/theme_available}}
          splashRadius: 10,
          value: areTermsChecked,
          onChanged: (value) {
            setState(() {
              widget.onTermsStateChanged(value);
              areTermsChecked = !areTermsChecked;
            });
          },
        ),
        RichText(
          textAlign: TextAlign.center,
          text: TextSpan(
            children: <TextSpan>[
              TextSpan(
                text: 'By continuing you agree with our \n',
                style: TextStyle(color: hexToColor('#A0AABD'), fontSize: 10.sp),
              ),
              getHighlitedHyperlink(
                'Terms and Conditions',
                AppString.termsAndConditionsText,
                context,
              ),
              TextSpan(
                text: ' and ',
                style: TextStyle(color: hexToColor('#A0AABD'), fontSize: 10.sp),
              ),
              getHighlitedHyperlink(
                'Privacy Policy',
                AppString.policyText,
                context,
              )
            ],
          ),
        ),
      ],
    );
  }
}

TextSpan getHighlitedHyperlink(
  String title,
  String text,
  BuildContext context,
) =>
    TextSpan(
      text: title,
      style: TextStyle(
        decoration: TextDecoration.underline,
          {{^theme_available}}color: Colors.orange[800],{{/theme_available}}
          {{#theme_available}}color: Theme.of(context).colorScheme.primary,{{/theme_available}}
        fontSize: 8.sp,
      ),
      recognizer: TapGestureRecognizer()
        ..onTap = () {
          showDialog(
            context: context,
            builder: (BuildContext context) => DetailsAlertDialog(
              title: title,
              text: text,
            ),
          );
        },
    );
