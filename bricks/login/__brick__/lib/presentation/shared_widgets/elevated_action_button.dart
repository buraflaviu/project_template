import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';

class ElevatedActionButton extends StatelessWidget {
  final Widget? icon;

  final Function onTap;

  final String text;

  final double height;

  final double width;

  final Color? backgroundColor;

  final bool isEnabled;

  final IconAlignment iconAlignment;

  final double? textSize;

  const ElevatedActionButton({
    Key? key,
    required this.onTap,
    required this.text,
    required this.width,
    required this.height,
    this.backgroundColor,
    this.icon,
    this.textSize,
    this.isEnabled = true,
    this.iconAlignment = IconAlignment.farFromText,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: width,
      height: height,
      child: ElevatedButton(
        // ignore: unnecessary_lambdas
        onPressed: isEnabled ? () => onTap.call() : null,
        style: ElevatedButton.styleFrom(
        {{^theme_available}}primary: backgroundColor ?? Colors.orange[800],{{/theme_available}}
        {{#theme_available}}primary: backgroundColor ?? Theme.of(context).colorScheme.primary,{{/theme_available}}
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(30),
          ),
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            if (icon != null)
              SizedBox(
                height: 20,
                width: 20,
                child: icon,
              ),
            Expanded(
              flex:
                  iconAlignment == IconAlignment.boundWithText ? 0 : 1,
              child: Text(
                text,
                textAlign: TextAlign.center,
    {{^theme_available}}style: TextStyle(fontSize: textSize??8.5.sp),{{/theme_available}}
    {{#theme_available}}  style: TextStyle(
    fontSize: textSize ?? 8.5.sp,
    color: Theme.of(context).colorScheme.onPrimary,
    ),{{/theme_available}}
              ),
            ),
          ],
        ),
      ),
    );
  }
}

enum IconAlignment {
  boundWithText,
  farFromText,
}
