part of 'localisation_bloc.dart';

@immutable
abstract class LocalisationEvent {}

@immutable
class LocalisationChangeLanguage extends LocalisationEvent {
  final String? languageName;

  LocalisationChangeLanguage({required this.languageName});
}

@immutable
class LocalisationLanguageForLogin extends LocalisationEvent{}

@immutable
class LocalisationLanguageAfterLogin extends LocalisationEvent{}
