part of 'video_recorder_bloc.dart';

@immutable
abstract class VideRecorderState {}

class VideRecorderInitialState extends VideRecorderState {}

class VideRecorderLoadingState extends VideRecorderState {}

class VideRecorderUploadFailedState extends VideRecorderState {
  final String message;

  VideRecorderUploadFailedState({
    required this.message,
  });
}

class VideRecorderUploadSuccessfullyState extends VideRecorderState {
  final String message;

  VideRecorderUploadSuccessfullyState({
    required this.message,
  });
}
