import 'package:dartz/dartz.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:injectable/injectable.dart';

import '../../core/errors/exceptions.dart';
import '../../core/errors/failures.dart';
import '../../core/utils/constants.dart';
import '../../core/utils/extensions.dart';
import '../../core/utils/logger.dart';
import '../../domain/repositories_contracts/authentication_repo.dart';
import '../datasource/remote/api/authentication_remote_api.dart';
import '../datasource/remote/components/custom_dio.dart';

@Singleton(as: AuthenticationRepo)
class AuthRepoImpl extends AuthenticationRepo {
  FlutterSecureStorage secureStorage = const FlutterSecureStorage();
  AuthenticationRemoteApi remoteAuthApi = AuthenticationRemoteApi();

  @override
  Future<Either<Failure, String>> loginClassic(
    String email,
    String password,
    bool isRememberMeAccepted,
  ) async {
    try {
      await CustomDio().refreshAccessInterceptors();
      return await remoteAuthApi.login(email, password).then((response) async {
        await secureStorage.write(
          key: AppKey.ACCESS_TOKEN,
          value: response.data['accessToken'] as String,
        );
        await secureStorage.write(
          key: AppKey.REFRESH_TOKEN,
          value: response.data['refreshToken'] as String,
        );

        if (isRememberMeAccepted) {
          await secureStorage.write(key: AppKey.USER_EMAIL, value: email);
          await secureStorage.write(key: AppKey.USER_PASSWORD, value: password);
        } else {
          await secureStorage.delete(key: AppKey.USER_EMAIL);
          await secureStorage.delete(key: AppKey.USER_PASSWORD);
        }
        Logger.success(runtimeType, AppLogger.credentialsStored);
        CustomDio().giveAccess();
        return Right(AppString.loginClassicSuccess.translate());
      });
    } on AppException catch (exception) {
      return Left(HardFailure(failureMessage: exception.message));
    } catch (e) {
      Logger.error(runtimeType, e.toString());
      return Left(
        HardFailure(
          failureMessage: AppString.unexpectedFailure.translate(),
        ),
      );
    }
  }

  @override
  Future<Either<Failure, String>> loginWithMicrosoft() async {
    try {
      await CustomDio().refreshAccessInterceptors();
      final response = await remoteAuthApi.logInWithMicrosoft();
      await secureStorage.write(
        key: AppKey.ACCESS_TOKEN,
        value: response.data['accessToken'] as String,
      );
      await secureStorage.write(
        key: AppKey.REFRESH_TOKEN,
        value: response.data['refreshToken'] as String,
      );

      Logger.success(runtimeType, AppLogger.microsoftAccessStored);
      CustomDio().giveAccess();
      return Right(AppString.microsoftSignInSuccess.translate());
    } catch (e) {
      return Left(
        ServerFailure(
          failureMessage: AppString.loginWithMicrosoftFailed.translate(),
        ),
      );
    }
  }

  @override
  Future<Either<Failure, String>> changePassword(
    String currentPassword,
    String newPassword,
  ) async {
    try {
      return await remoteAuthApi
          .changePassword(
        currentPassword,
        newPassword,
      )
          .then((response) async {
        await secureStorage.write(
          key: AppKey.USER_PASSWORD,
          value: newPassword,
        );

        Logger.success(runtimeType, AppLogger.newPasswordSaved);
        return Right(AppString.newPasswordSaved.translate());
      });
    } on AppException catch (exception) {
      return Left(HardFailure(failureMessage: exception.message));
    } catch (e) {
      Logger.error(runtimeType, e.toString());
      return Left(
        HardFailure(failureMessage: AppString.unexpectedFailure.translate()),
      );
    }
  }
}
