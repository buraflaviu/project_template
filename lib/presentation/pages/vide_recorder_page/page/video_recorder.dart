import 'dart:io';

import 'package:camera/camera.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:video_player/video_player.dart';

import '../../../../data/repositories_imp/video_recorder_repo_impl.dart';
import '../bloc/video_recorder_bloc.dart';
import 'camera_page.dart';

class VideRecordingPage extends StatefulWidget {
  final List<CameraDescription> cameras;

  const VideRecordingPage({Key? key, required this.cameras}) : super(key: key);

  @override
  State<VideRecordingPage> createState() => _VideRecordingPageState();
}

class _VideRecordingPageState extends State<VideRecordingPage> {
  final _bloc =
      VideRecorderBloc(videoRecorderRepository: VideoRecorderRepositoryImpl());

  bool weHaveARecordedVideo = false;
  VideoPlayerController? videoController;
  XFile? videoFile;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: BlocListener(
          bloc: _bloc,
          listener: (context, state) {
            if (state is VideRecorderUploadFailedState) {
              //todo something with it
            }
            if (state is VideRecorderUploadSuccessfullyState) {
              //todo something with it
            }
          },
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              !weHaveARecordedVideo
                  ? const Text('No video recorded')
                  : SizedBox(
                      width: 400,
                      height: 400,
                      child: Center(
                        child: AspectRatio(
                            aspectRatio: videoController!.value.aspectRatio,
                            child: VideoPlayer(videoController!)),
                      ),
                    ),
              FloatingActionButton(
                  child: const Text("Record"),
                  onPressed: () async {
                    videoFile = await Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => CameraPage(
                                cameras: widget.cameras,
                              )),
                    );
                    _startVideoPlayer();
                  }),
              if (weHaveARecordedVideo)
                FloatingActionButton(
                    child: const Text("Upload this video to server"),
                    onPressed: () async {
                      _bloc.add(VideRecorderUploadVideoEvent(
                          path: videoFile!.path, name: videoFile!.name));
                    }),
            ],
          ),
        ),
      ),
    );
  }

  Future<void> _startVideoPlayer() async {
    if (videoFile == null) {
      return;
    }

    final VideoPlayerController vController =
        VideoPlayerController.file(File(videoFile!.path));
    await vController.initialize();
    await vController.setLooping(true);
    await videoController?.dispose();
    if (mounted) {
      setState(() {
        videoController = vController;
      });
    }
    await vController.play();
    setState(() {
      weHaveARecordedVideo = true;
    });
  }
}
