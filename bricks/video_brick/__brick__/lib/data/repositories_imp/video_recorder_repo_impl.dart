import 'package:dartz/dartz.dart';

import '../../core/errors/exceptions.dart';
import '../../core/errors/failures.dart';
import '../../domain/repositories_contracts/video_recorder_repo.dart';
import '../datasource/remote/api/video_recorder_remote_api.dart';

class VideoRecorderRepositoryImpl extends VideoRecorderRepository {
VideoRecorderRemoteApi remoteApi = VideoRecorderRemoteApi();

  @override
  Future<Either<Failure, String>> uploadVideo(String path, String name) async {
    try {
      final message = await remoteApi.uploadVideo(path, name);
      return Right(message);
    } on AppException catch (appException) {
      return Left(HardFailure(failureMessage: appException.message));
    } catch (exception) {
      return Left(ServerFailure(failureMessage: exception.toString()));
    }
  }
}
