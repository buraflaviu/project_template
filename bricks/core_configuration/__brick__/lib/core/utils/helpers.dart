import 'dart:io';

import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:sizer/sizer.dart';

import 'constants.dart';
import 'extensions.dart';

Color hexToColor(String hexString, {String alphaChannel = 'FF'}) {
  return Color(int.parse(hexString.replaceFirst('#', '0x$alphaChannel')));
}

int colorFromHex(String hexColor) {
  var color = hexColor.toUpperCase().replaceAll('#', '');
  if (color.length == 6) {
    color = 'FF$color';
  }
  return int.parse(color, radix: 16);
}

String formatDate(int currentMonth, int currentDay, int currentYear) {
  String month, day;
  if (currentMonth < 10) {
    month = '0$currentMonth.';
  } else {
    month = '$currentMonth.';
  }
  if (currentDay < 10) {
    day = '0$currentDay.';
  } else {
    day = '$currentDay.';
  }
  return '$day$month$currentYear';
}

Widget fromNameToUserPhoto(String name) {
  if (name.isNotEmpty && name.isNotEmptyString) {
    final nameArray = name.trim().split(' ');
    return Text(
      nameArray.first[0] + nameArray.last[0],
      style: TextStyle(fontSize: 14.sp),
    );
  } else {
    return Image.asset(AppPaths.profileAvatar);
  }
}

List<String> splitTimeIntoDigits(int? time) {
  final List<String> digitArray = [];
  if (time == null) {
    digitArray.add('0');
  } else {
    final String res = time.toString();
    for (final c in res.runes) {
      digitArray.add(String.fromCharCode(c));
    }
  }
  return digitArray;
}

String formatFilterData(DateTime dateTime) =>
    DateFormat('dd.MM.yyyy').format(dateTime);

String dateTimeToDayText(DateTime dateTime) {
  var suffix = 'th';
  final digit = dateTime.day % 10;
  if ((digit > 0 && digit < 4) && (dateTime.day < 11 || dateTime.day > 13)) {
    suffix = ['st', 'nd', 'rd'][digit - 1];
  }
  return DateFormat("MMMM, d'$suffix'").format(dateTime).toUpperCase();
}

String dateTimeToString(DateTime dateTime) {
  var suffix = 'th';
  final digit = dateTime.day % 10;
  if ((digit > 0 && digit < 4) && (dateTime.day < 11 || dateTime.day > 13)) {
    suffix = ['st', 'nd', 'rd'][digit - 1];
  }
  return DateFormat("d'$suffix' of MMM yyyy").format(dateTime);
}

String formatDateForApiCalls(DateTime date) =>
    DateFormat('dd.MM.yyyy').format(date);

String formatDateToTime(DateTime date) {
  return DateFormat('Hm').format(date);
}

String formatDateForGraph(DateTime date) {
  return DateFormat('MMMM yyyy').format(date);
}

String computeDuration(DateTime startTime, DateTime endTime) {
  final DateTime duration = endTime.subtract(
    Duration(
      hours: startTime.hour,
      minutes: startTime.minute,
      seconds: startTime.second,
    ),
  );

  String formattedDuration = DateFormat('H').format(duration);
  formattedDuration = '${formattedDuration}h';

  if (formattedDuration[0] == '0') {
    return formattedDuration.substring(1);
  }

  return formattedDuration;
}

DateTime setStartDateTime(String string) {
  final newDate = DateTime.parse(string);
  return DateTime(newDate.year, newDate.month, newDate.day, 6);
}

DateTime setEndDateTime(String string) {
  final endDate = DateTime.parse(string);
  return DateTime(endDate.year, endDate.month, endDate.day, 22);
}
