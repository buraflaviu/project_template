import '../utils/logger.dart';

abstract class AppException implements Exception {
  final String message;

  AppException(this.message);
}

class EasyException extends AppException {
  EasyException(String message) : super(message);
}

class MediumException extends AppException {
  MediumException(dynamic type, String message) : super(message) {
    Logger.warning(type, message);
  }
}

class HardException extends AppException {
  HardException(dynamic type, String message) : super(message) {
    Logger.error(type, message);
  }
}
