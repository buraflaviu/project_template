part of '{{pageName.snakeCase()}}_bloc.dart';

@immutable
abstract class {{pageName.pascalCase()}}State {}

class {{pageName.pascalCase()}}InitialState extends {{pageName.pascalCase()}}State {}

class {{pageName.pascalCase()}}LoadingState extends {{pageName.pascalCase()}}State {}

class {{pageName.pascalCase()}}CompleteTaskLoadingState extends {{pageName.pascalCase()}}State {}

class {{pageName.pascalCase()}}CompleteTaskSuccessState extends {{pageName.pascalCase()}}State {}

class {{pageName.pascalCase()}}DataFetchedFailedState extends {{pageName.pascalCase()}}State {
  final String message;

{{pageName.pascalCase()}}DataFetchedFailedState({
    required this.message,
  });
}

class {{pageName.pascalCase()}}CompleteTaskFailedState extends {{pageName.pascalCase()}}State {
  final String message;

{{pageName.pascalCase()}}CompleteTaskFailedState({
    required this.message,
  });
}

class {{pageName.pascalCase()}}DataFetchedState extends {{pageName.pascalCase()}}State {
  final int userId;

  final String taskDescription;

  final bool isStatusCompleted;

{{pageName.pascalCase()}}DataFetchedState({
    required this.userId,
    required this.taskDescription,
    required this.isStatusCompleted,
  });
}
