class UserInfoEntity {
  final int userId;

  final String description;

  final bool isTaskCompleted;

  UserInfoEntity({
    required this.userId,
    required this.description,
    required this.isTaskCompleted,
  });
}
