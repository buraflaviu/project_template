import 'package:dartz/dartz.dart';

import '../../core/errors/failures.dart';

abstract class LocalisationRepository {
  Future changeCurrentLanguage(String? languageName);

  Future loadLanguageForLogin();

  Future loadTranslationAfterLogin();

  Future<Either<Failure, List<String>>> fetchAllLanguages();
}