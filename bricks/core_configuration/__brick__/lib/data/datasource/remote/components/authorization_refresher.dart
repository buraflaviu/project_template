import 'dart:async';

import 'package:dio/dio.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

import '../../../../core/utils/constants.dart';
import '../../../../core/utils/logger.dart';
import 'custom_dio.dart';

class AuthorizationRefresher {
  Timer? refreshAccessTimer;

  void giveAccess() {
    refreshAccessTimer = Timer.periodic(
      Duration(seconds: 540),
      (Timer timer) async => _refreshExpiredToken(),
    );
    Logger.info(runtimeType, AppLogger.accessTimerStarted);
  }

  void stopAccess() {
    if (refreshAccessTimer != null) {
      refreshAccessTimer!.cancel();
      Logger.info(runtimeType, AppLogger.accessTimerStopped);
    }
  }

  Future<bool> _refreshExpiredToken() async {
    await dotenv.load();
    final String refreshTokenURL = dotenv.env[AppConst.REFRESH_TOKEN_URL]!;
    final String refreshToken =
        await FlutterSecureStorage().read(key: AppKey.REFRESH_TOKEN) ??
            AppConst.EMPTY_STRING;

    final dio = CustomDio();
    final formData = FormData.fromMap({
      'GrantType': 'refresh_token',
      'RefreshToken': refreshToken,
    });
    final response = await dio.makeSecureCall(
      () => dio.instance.postUri(
        Uri.parse(refreshTokenURL),
        data: formData,
      ),
    );
    return response.fold((fail) => false, (response) async {
      if (response.statusCode == 200) {
        await FlutterSecureStorage().write(
          key: AppKey.ACCESS_TOKEN,
          value: response.data['accessToken'] as String,
        );
        await FlutterSecureStorage().write(
          key: AppKey.REFRESH_TOKEN,
          value: response.data['refreshToken'] as String,
        );
        Logger.success(runtimeType, AppLogger.tokenRefreshed);
        return true;
      }
      Logger.error(runtimeType, AppString.refreshTokenFailed);
      return false;
    });
  }
}
