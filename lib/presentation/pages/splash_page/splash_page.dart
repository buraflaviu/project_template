import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';
import '../login/login_page.dart';
import '../../../core/utils/constants.dart';

class SplashPage extends StatefulWidget {
  const SplashPage({Key? key}) : super(key: key);

  @override
  State<SplashPage> createState() => _SplashPageState();
}

class _SplashPageState extends State<SplashPage> {

   @override
  void initState() {
    Future.delayed(Duration(seconds: 3), () {
  Navigator.of(context).push(
  MaterialPageRoute(
  builder: (context) => LoginPage(),
  ),
  );
    });
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      child: Column(
        children: [
          Expanded(
            flex: 9,
            child: Image.asset(AppPaths.shiftingPromoteLogo),
          ),
          Flexible(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  'powered by ',
                  style: TextStyle(
                    fontFamily: 'calibri',
                    fontSize: 8.sp,
                    color: Colors.black,
                    fontWeight: FontWeight.normal,
                    decoration: TextDecoration.none,
                  ),
                ),
                Image.asset(AppPaths.htssLogo),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
