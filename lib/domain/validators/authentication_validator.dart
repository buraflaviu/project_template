import 'package:dartz/dartz.dart';
import 'package:injectable/injectable.dart';

import '../../../core/errors/failures.dart';
import '../../../core/utils/constants.dart';
import '../../../core/utils/extensions.dart';

@injectable
class AuthenticationValidator {
  bool _isEmailValid(String? email) {
    if (email == null || email.isEmpty) return false;
    return true;
  }

  bool _isPasswordValid(String? password) {
    if (password == null || password.isEmpty) return false;
    return true;
  }

  Either<Failure, void> checkTermsCondition(bool areTermsAccepted) {
    if (!areTermsAccepted) {
      return left(
        FieldFailure(
          failureMessage: AppString.termsNotCheckedError.translate(),
        ),
      );
    }
    return right(null);
  }

  Either<Failure, void> areLoginInformationValid(
    String? email,
    String? password,
    bool areTermsAccepted,
  ) {
    if (!_isEmailValid(email) || !_isPasswordValid(password)) {
      return left(
        FieldFailure(
          failureMessage: AppString.credentialsNotValidError.translate(),
        ),
      );
    }
    return checkTermsCondition(areTermsAccepted);
  }

  Either<Failure, void> isNewPasswordValid(
    String? currentPassword,
    String? newPassword,
    String? confirmPassword,
  ) {
    if (!_isPasswordValid(newPassword) ||
        !_isPasswordValid(confirmPassword) ||
        !_isPasswordValid(currentPassword)) {
      return left(
        FieldFailure(failureMessage: AppString.passwordsAreNull.translate()),
      );
    }
    if (currentPassword == newPassword) {
      return left(
        FieldFailure(
          failureMessage: AppString.changePasswordFailedError.translate(),
        ),
      );
    }
    if (confirmPassword != newPassword) {
      return left(
        FieldFailure(
          failureMessage: AppString.passwordsAreNotMatching.translate(),
        ),
      );
    }

    return right(null);
  }
}
