import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';

class DetailsAlertDialog extends StatelessWidget {
  final String title;

  final String text;

  const DetailsAlertDialog({
    Key? key,
    required this.title,
    required this.text,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        StatefulBuilder(
          builder: (BuildContext context, _) {
            return AlertDialog(
              contentPadding:
                  EdgeInsets.symmetric(horizontal: 3.w, vertical: 2.h),
              title: Center(
                child: Text(
                  title,
                  style: TextStyle(
                    fontSize: 20,
                    color: Colors.black,
                  ),
                ),
              ),
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(10)),
              ),
              content: SingleChildScrollView(
                child: Text(
                  text,
                  style: TextStyle(
                    fontSize: 12.sp,
                    color: Colors.black,
                  ),
                ),
              ),
            );
          },
        ),
      ],
    );
  }
}
