import 'package:dartz/dartz.dart';

import '../../core/errors/failures.dart';
import '../entities/user_info_entity.dart';

abstract class {{pageName.pascalCase()}}Repository {
  Future<Either<Failure, UserInfoEntity>> getUserInfo();

  Future<Either<Failure, String>> completeTask(bool isTaskToBeCompleted);
}
