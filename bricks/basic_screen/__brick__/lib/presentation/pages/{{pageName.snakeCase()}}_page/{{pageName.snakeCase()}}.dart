import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../data/repositories_imp/{{pageName.snakeCase()}}_repo_impl.dart';
import '../../pages/{{pageName.snakeCase()}}_page/bloc/{{pageName.snakeCase()}}_bloc.dart';

class {{pageName.pascalCase()}} extends StatefulWidget {
  const {{pageName.pascalCase()}}({Key? key}) : super(key: key);

  @override
  State<{{pageName.pascalCase()}}> createState() => _{{pageName.pascalCase()}}State();
}

class _{{pageName.pascalCase()}}State extends State<{{pageName.pascalCase()}}> {
  final _bloc = {{pageName.pascalCase()}}Bloc({{pageName.camelCase()}}Repository: {{pageName.pascalCase()}}RepositoryImpl())
    ..add({{pageName.pascalCase()}}FetchDataEvent());

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 10),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            BlocConsumer<{{pageName.pascalCase()}}Bloc, {{pageName.pascalCase()}}State>(
              bloc: _bloc,
              listenWhen: (_, state) => state is {{pageName.pascalCase()}}DataFetchedFailedState,
              listener: (context, state) {
                if (state is {{pageName.pascalCase()}}DataFetchedFailedState) {
                  //do something with this error
                }
              },
              buildWhen: (_, state) =>
                  state is {{pageName.pascalCase()}}DataFetchedState ||
                  state is {{pageName.pascalCase()}}LoadingState,
              builder: (context, state) {
                if (state is {{pageName.pascalCase()}}DataFetchedState) {
                  return Column(
                    children: [
                      Text('Hello user with id: ${state.userId}'),
                      Text('Task description: ${state.taskDescription}'),
                      Text('Task completed: ${state.isStatusCompleted}')
                    ],
                  );
                } else {
                  return Center(
                    child: CircularProgressIndicator(),
                  );
                }
              },
            ),
            BlocBuilder<{{pageName.pascalCase()}}Bloc, {{pageName.pascalCase()}}State>(
              bloc: _bloc,
              builder: (context, state) {
                if (state is {{pageName.pascalCase()}}CompleteTaskLoadingState) {
                  return Center(
                    child: CircularProgressIndicator(),
                  );
                }
                return Center(
                  child: ElevatedButton(
                    onPressed: () {
                      _bloc.add({{pageName.pascalCase()}}CompleteTaskEvent(completeTask: true));
                    },
                    child: Text('Send a post request'),
                  ),
                );
              },
            ),
          ],
        ),
      ),
    );
  }
}
