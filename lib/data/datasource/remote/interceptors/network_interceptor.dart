import 'dart:io';

import 'package:dio/dio.dart';

import '../../../../core/errors/exceptions.dart';
import '../../../../core/utils/constants.dart';
import '../../../../core/utils/extensions.dart';
import '../../../../core/utils/logger.dart';

class NetworkInterceptor extends Interceptor {
  @override
  Future<void> onError(DioError err, ErrorInterceptorHandler handler) async {
    Logger.info(runtimeType, AppLogger.loggerNetworkInterceptorActivated);
    try {
      final result = await InternetAddress.lookup('www.google.com');
      if (result.isNotEmpty && result.first.rawAddress.isNotEmpty) {
        Logger.info(runtimeType, AppLogger.internetConnectionStable);
        super.onError(err, handler);
      } else {
        throw HardException(
          runtimeType,
          AppString.noInternetConnection,
        );
      }
    } catch (exception) {
      String errorMessage = AppString.unexpectedFailure;
      if (exception is SocketException) {
        errorMessage = AppString.noNetworkToPeripherals;
      }
      if (exception is AppException) {
        errorMessage = exception.message;
      } else {
        Logger.error(runtimeType, errorMessage);
      }
      handler.reject(
        DioError(
          type: DioErrorType.cancel,
          requestOptions: err.requestOptions,
          error: errorMessage,
        ),
      );
    }
  }
}
