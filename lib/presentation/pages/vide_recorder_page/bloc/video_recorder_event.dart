part of 'video_recorder_bloc.dart';

@immutable
abstract class VideRecorderEvent {}


class VideRecorderUploadVideoEvent extends VideRecorderEvent {
  final String path;
  final String name;
  VideRecorderUploadVideoEvent({required this.path, required this.name});
}
