import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:sizer/sizer.dart';
{{#theme_available}}import 'presentation/bloc/theme_bloc/theme_bloc.dart';{{/theme_available}}
import 'core/di/injectable.dart';
{{#splash_available}}import '/presentation/pages/splash_page/splash_page.dart';{{/splash_available}}
{{#login_available}}import 'presentation/bloc/authentication_bloc/authentication_bloc.dart';
import '/presentation/pages/login/login_page.dart';{{/login_available}}
{{#localisation_available}}import 'presentation/bloc/localisation_bloc/localisation_bloc.dart';{{/localisation_available}}

Future<void> main() async {
  HttpOverrides.global = MyHttpOverrides();
  configureDependencies();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    {{#login_available}}
    return MultiBlocProvider(
    providers: [
    BlocProvider<AuthenticationBloc>(
    create: (BuildContext context) =>
    getIt<AuthenticationBloc>()..add(LoginFetchDataEvent()), ),
    {{#theme_available}}
    BlocProvider<ThemeBloc>(
    create: (BuildContext context) => getIt<ThemeBloc>(),
    ),
    {{/theme_available}}
    {{#localisation_available}}
    BlocProvider<LocalisationBloc>(
    create: (BuildContext context) => getIt<LocalisationBloc>()..add(LocalisationLanguageForLogin()),),
    {{/localisation_available}}
    ],
    child:
    {{#localisation_available}}
    BlocBuilder
    <LocalisationBloc, LocalisationState>(
    builder: (context, state) {
    return Sizer(
    builder: (context, orientation, deviceType) {
      {{#theme_available}}
      return BlocBuilder<ThemeBloc, ThemeState>(
      builder: (context, state) {
      return MaterialApp(
      theme: state.themeData,
      builder: EasyLoading.init(),
      home:{{#login_available}}{{#splash_available}}SplashPage(),{{/splash_available}}{{^splash_available}}LoginPage(),{{/splash_available}}{{/login_available}}
      );
      },
      );
      {{/theme_available}}
      {{^theme_available}}
    return MaterialApp(
    builder: EasyLoading.init(),
    home:{{#login_available}}{{#splash_available}}SplashPage(),{{/splash_available}}{{^splash_available}}LoginPage(),{{/splash_available}}{{/login_available}}
    );
    {{/theme_available}}

    },
    );
    },
    ),
    {{/localisation_available}}
    {{^localisation_available}}
    Sizer(
    builder: (context, orientation, deviceType) {
    {{#theme_available}}
    return BlocBuilder<ThemeBloc, ThemeState>(
    builder: (context, state) {
    return MaterialApp(
    theme: state.themeData,
    builder: EasyLoading.init(),
    home:{{#login_available}}{{#splash_available}}SplashPage(),{{/splash_available}}{{^splash_available}}LoginPage(),{{/splash_available}}{{/login_available}}
    );
    },
    );
    {{/theme_available}}
    {{^theme_available}}
    return MaterialApp(
    builder: EasyLoading.init(),
    home:{{#login_available}}{{#splash_available}}SplashPage(),{{/splash_available}}{{^splash_available}}LoginPage(),{{/splash_available}}{{/login_available}}
    );
    {{/theme_available}}

    },
    ),
    {{/localisation_available}}
    );
    {{/login_available}}

    {{^login_available}}
    {{#localisation_available}}
    return MultiBlocProvider(
    providers: [
    BlocProvider
    <LocalisationBloc>(
    create: (BuildContext context) => getIt
    <LocalisationBloc>()
    ..add(LocalisationLanguageForLogin()),),
    {{#theme_available}}
    BlocProvider<ThemeBloc>(
    create: (BuildContext context) => getIt<ThemeBloc>(),
    ),
    {{/theme_available}}
    ],
    child:  BlocBuilder
    <LocalisationBloc, LocalisationState>(
    builder: (context, state) {
    return Sizer(
    builder: (context, orientation, deviceType) {
    {{#theme_available}}
    return BlocBuilder<ThemeBloc, ThemeState>(
    builder: (context, state) {
    return MaterialApp(
    theme: state.themeData,
    builder: EasyLoading.init(),
    home:{{#login_available}}{{#splash_available}}SplashPage(),{{/splash_available}}{{^splash_available}}LoginPage(),{{/splash_available}}{{/login_available}}
    );
    },
    );
    {{/theme_available}}

    {{^theme_available}}
    return MaterialApp(
    builder: EasyLoading.init(),
    home:{{#login_available}}{{#splash_available}}SplashPage(),{{/splash_available}}{{^splash_available}}LoginPage(),{{/splash_available}}{{/login_available}}
    );
    {{/theme_available}}

    },
    );
    },
    ),);
    {{/localisation_available}}
    {{^localisation_available}}
    return Sizer(
    builder: (context, orientation, deviceType) {
    {{#theme_available}}
    return BlocBuilder<ThemeBloc, ThemeState>(
    builder: (context, state) {
    return MaterialApp(
    theme: state.themeData,
    builder: EasyLoading.init(),
    home:{{#login_available}}{{#splash_available}}SplashPage(),{{/splash_available}}{{^splash_available}}LoginPage(),{{/splash_available}}{{/login_available}}
    );
    },
    );
    {{/theme_available}}
    {{^theme_available}}
    return MaterialApp(
    builder: EasyLoading.init(),
  home:{{#login_available}}{{#splash_available}}SplashPage(),{{/splash_available}}{{^splash_available}}LoginPage(),{{/splash_available}}{{/login_available}}
    );
    {{/theme_available}}
    },);
    {{/localisation_available}}
    {{/login_available}}
  }
}

class MyHttpOverrides extends HttpOverrides {
  @override
  HttpClient createHttpClient(SecurityContext? context) {
    return super.createHttpClient(context)
      ..badCertificateCallback =
          (X509Certificate cert, String host, int port) => true;
  }
}
