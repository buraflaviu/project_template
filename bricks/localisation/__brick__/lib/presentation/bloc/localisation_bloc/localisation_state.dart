part of 'localisation_bloc.dart';

@immutable
abstract class LocalisationState {}

class LocalisationInitial extends LocalisationState {}

class LocalisationLanguageChanged extends LocalisationState {}

class LocalisationBeforeLoginFetched extends LocalisationState {}

class LocalisationAfterLoginFetched extends LocalisationState {}

class LocalisationLoading extends LocalisationState {}

