import 'dart:async';

import 'package:dartz/dartz.dart';
import 'package:dio/dio.dart';
import 'package:pretty_dio_logger/pretty_dio_logger.dart';

import '../../../../core/errors/failures.dart';
import '../../../../core/utils/constants.dart';
import '../../../../core/utils/extensions.dart';
import '../../../../core/utils/logger.dart';
import '../interceptors/network_interceptor.dart';
import '../interceptors/request_handler_interceptor.dart';
import 'authorization_refresher.dart';

class CustomDio with AuthorizationRefresher {
  static CustomDio? singletonHttp;
  late Dio instance;

  factory CustomDio() {
    singletonHttp ??= CustomDio.internal();
    return singletonHttp!;
  }

  CustomDio.internal() {
    instance = Dio()
      ..interceptors.addAll([
        // PrettyDioLogger(
        //   responseHeader: true,
        // ),
        NetworkInterceptor(),
        RequestHandlerInterceptor(),
      ]);
    instance.options.connectTimeout = 5000;
    instance.options.receiveTimeout = 5000;
  }

  Future<Either<Failure, Response>> makeSecureCall(Function request) async {
    try {
      final response = await request.call() as Response;
      return Right(response);
    } on DioError catch (dioError) {
      String errorMessage = dioError.message;
      if (dioError.type == DioErrorType.connectTimeout) {
        errorMessage = AppString.problemWithTheConnectionError;
      }
      Logger.warning('$runtimeType ${dioError.runtimeType}', errorMessage);
      return Left(NetworkFailure(failureMessage: errorMessage));
    } catch (exception) {
      Logger.warning(runtimeType, exception);
      return Left(
        ServerFailure(
          failureMessage: AppString.unexpectedFailure,
        ),
      );
    }
  }

  Future refreshAccessInterceptors() async {
    await (instance.interceptors.firstWhere(
      (element) => element.runtimeType == RequestHandlerInterceptor,
    ) as RequestHandlerInterceptor)
        .revokeAccess();
  }
}
