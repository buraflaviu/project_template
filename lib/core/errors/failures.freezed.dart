// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'failures.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
class _$FailureTearOff {
  const _$FailureTearOff();

  NoDataFetched<T> noDataFetched<T>({required String failureMessage}) {
    return NoDataFetched<T>(
      failureMessage: failureMessage,
    );
  }

  ServerFailure<T> serverFailure<T>({required String failureMessage}) {
    return ServerFailure<T>(
      failureMessage: failureMessage,
    );
  }

  FieldFailure<T> fieldFailure<T>({required String failureMessage}) {
    return FieldFailure<T>(
      failureMessage: failureMessage,
    );
  }

  NetworkFailure<T> networkFailure<T>({required String failureMessage}) {
    return NetworkFailure<T>(
      failureMessage: failureMessage,
    );
  }

  EasyFailure<T> easyImpactFailure<T>({required String failureMessage}) {
    return EasyFailure<T>(
      failureMessage: failureMessage,
    );
  }

  MediumFailure<T> mediumImpactFailure<T>({required String failureMessage}) {
    return MediumFailure<T>(
      failureMessage: failureMessage,
    );
  }

  HardFailure<T> hardImpactFailure<T>({required String failureMessage}) {
    return HardFailure<T>(
      failureMessage: failureMessage,
    );
  }
}

/// @nodoc
const $Failure = _$FailureTearOff();

/// @nodoc
mixin _$Failure<T> {
  String get failureMessage => throw _privateConstructorUsedError;

  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String failureMessage) noDataFetched,
    required TResult Function(String failureMessage) serverFailure,
    required TResult Function(String failureMessage) fieldFailure,
    required TResult Function(String failureMessage) networkFailure,
    required TResult Function(String failureMessage) easyImpactFailure,
    required TResult Function(String failureMessage) mediumImpactFailure,
    required TResult Function(String failureMessage) hardImpactFailure,
  }) =>
      throw _privateConstructorUsedError;

  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(String failureMessage)? noDataFetched,
    TResult Function(String failureMessage)? serverFailure,
    TResult Function(String failureMessage)? fieldFailure,
    TResult Function(String failureMessage)? networkFailure,
    TResult Function(String failureMessage)? easyImpactFailure,
    TResult Function(String failureMessage)? mediumImpactFailure,
    TResult Function(String failureMessage)? hardImpactFailure,
  }) =>
      throw _privateConstructorUsedError;

  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String failureMessage)? noDataFetched,
    TResult Function(String failureMessage)? serverFailure,
    TResult Function(String failureMessage)? fieldFailure,
    TResult Function(String failureMessage)? networkFailure,
    TResult Function(String failureMessage)? easyImpactFailure,
    TResult Function(String failureMessage)? mediumImpactFailure,
    TResult Function(String failureMessage)? hardImpactFailure,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;

  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(NoDataFetched<T> value) noDataFetched,
    required TResult Function(ServerFailure<T> value) serverFailure,
    required TResult Function(FieldFailure<T> value) fieldFailure,
    required TResult Function(NetworkFailure<T> value) networkFailure,
    required TResult Function(EasyFailure<T> value) easyImpactFailure,
    required TResult Function(MediumFailure<T> value) mediumImpactFailure,
    required TResult Function(HardFailure<T> value) hardImpactFailure,
  }) =>
      throw _privateConstructorUsedError;

  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(NoDataFetched<T> value)? noDataFetched,
    TResult Function(ServerFailure<T> value)? serverFailure,
    TResult Function(FieldFailure<T> value)? fieldFailure,
    TResult Function(NetworkFailure<T> value)? networkFailure,
    TResult Function(EasyFailure<T> value)? easyImpactFailure,
    TResult Function(MediumFailure<T> value)? mediumImpactFailure,
    TResult Function(HardFailure<T> value)? hardImpactFailure,
  }) =>
      throw _privateConstructorUsedError;

  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(NoDataFetched<T> value)? noDataFetched,
    TResult Function(ServerFailure<T> value)? serverFailure,
    TResult Function(FieldFailure<T> value)? fieldFailure,
    TResult Function(NetworkFailure<T> value)? networkFailure,
    TResult Function(EasyFailure<T> value)? easyImpactFailure,
    TResult Function(MediumFailure<T> value)? mediumImpactFailure,
    TResult Function(HardFailure<T> value)? hardImpactFailure,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $FailureCopyWith<T, Failure<T>> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $FailureCopyWith<T, $Res> {
  factory $FailureCopyWith(Failure<T> value, $Res Function(Failure<T>) then) =
      _$FailureCopyWithImpl<T, $Res>;

  $Res call({String failureMessage});
}

/// @nodoc
class _$FailureCopyWithImpl<T, $Res> implements $FailureCopyWith<T, $Res> {
  _$FailureCopyWithImpl(this._value, this._then);

  final Failure<T> _value;

  // ignore: unused_field
  final $Res Function(Failure<T>) _then;

  @override
  $Res call({
    Object? failureMessage = freezed,
  }) {
    return _then(_value.copyWith(
      failureMessage: failureMessage == freezed
          ? _value.failureMessage
          : failureMessage // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc
abstract class $NoDataFetchedCopyWith<T, $Res>
    implements $FailureCopyWith<T, $Res> {
  factory $NoDataFetchedCopyWith(
          NoDataFetched<T> value, $Res Function(NoDataFetched<T>) then) =
      _$NoDataFetchedCopyWithImpl<T, $Res>;

  @override
  $Res call({String failureMessage});
}

/// @nodoc
class _$NoDataFetchedCopyWithImpl<T, $Res>
    extends _$FailureCopyWithImpl<T, $Res>
    implements $NoDataFetchedCopyWith<T, $Res> {
  _$NoDataFetchedCopyWithImpl(
      NoDataFetched<T> _value, $Res Function(NoDataFetched<T>) _then)
      : super(_value, (v) => _then(v as NoDataFetched<T>));

  @override
  NoDataFetched<T> get _value => super._value as NoDataFetched<T>;

  @override
  $Res call({
    Object? failureMessage = freezed,
  }) {
    return _then(NoDataFetched<T>(
      failureMessage: failureMessage == freezed
          ? _value.failureMessage
          : failureMessage // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$NoDataFetched<T> implements NoDataFetched<T> {
  const _$NoDataFetched({required this.failureMessage});

  @override
  final String failureMessage;

  @override
  String toString() {
    return 'Failure<$T>.noDataFetched(failureMessage: $failureMessage)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is NoDataFetched<T> &&
            const DeepCollectionEquality()
                .equals(other.failureMessage, failureMessage));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType, const DeepCollectionEquality().hash(failureMessage));

  @JsonKey(ignore: true)
  @override
  $NoDataFetchedCopyWith<T, NoDataFetched<T>> get copyWith =>
      _$NoDataFetchedCopyWithImpl<T, NoDataFetched<T>>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String failureMessage) noDataFetched,
    required TResult Function(String failureMessage) serverFailure,
    required TResult Function(String failureMessage) fieldFailure,
    required TResult Function(String failureMessage) networkFailure,
    required TResult Function(String failureMessage) easyImpactFailure,
    required TResult Function(String failureMessage) mediumImpactFailure,
    required TResult Function(String failureMessage) hardImpactFailure,
  }) {
    return noDataFetched(failureMessage);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(String failureMessage)? noDataFetched,
    TResult Function(String failureMessage)? serverFailure,
    TResult Function(String failureMessage)? fieldFailure,
    TResult Function(String failureMessage)? networkFailure,
    TResult Function(String failureMessage)? easyImpactFailure,
    TResult Function(String failureMessage)? mediumImpactFailure,
    TResult Function(String failureMessage)? hardImpactFailure,
  }) {
    return noDataFetched?.call(failureMessage);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String failureMessage)? noDataFetched,
    TResult Function(String failureMessage)? serverFailure,
    TResult Function(String failureMessage)? fieldFailure,
    TResult Function(String failureMessage)? networkFailure,
    TResult Function(String failureMessage)? easyImpactFailure,
    TResult Function(String failureMessage)? mediumImpactFailure,
    TResult Function(String failureMessage)? hardImpactFailure,
    required TResult orElse(),
  }) {
    if (noDataFetched != null) {
      return noDataFetched(failureMessage);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(NoDataFetched<T> value) noDataFetched,
    required TResult Function(ServerFailure<T> value) serverFailure,
    required TResult Function(FieldFailure<T> value) fieldFailure,
    required TResult Function(NetworkFailure<T> value) networkFailure,
    required TResult Function(EasyFailure<T> value) easyImpactFailure,
    required TResult Function(MediumFailure<T> value) mediumImpactFailure,
    required TResult Function(HardFailure<T> value) hardImpactFailure,
  }) {
    return noDataFetched(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(NoDataFetched<T> value)? noDataFetched,
    TResult Function(ServerFailure<T> value)? serverFailure,
    TResult Function(FieldFailure<T> value)? fieldFailure,
    TResult Function(NetworkFailure<T> value)? networkFailure,
    TResult Function(EasyFailure<T> value)? easyImpactFailure,
    TResult Function(MediumFailure<T> value)? mediumImpactFailure,
    TResult Function(HardFailure<T> value)? hardImpactFailure,
  }) {
    return noDataFetched?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(NoDataFetched<T> value)? noDataFetched,
    TResult Function(ServerFailure<T> value)? serverFailure,
    TResult Function(FieldFailure<T> value)? fieldFailure,
    TResult Function(NetworkFailure<T> value)? networkFailure,
    TResult Function(EasyFailure<T> value)? easyImpactFailure,
    TResult Function(MediumFailure<T> value)? mediumImpactFailure,
    TResult Function(HardFailure<T> value)? hardImpactFailure,
    required TResult orElse(),
  }) {
    if (noDataFetched != null) {
      return noDataFetched(this);
    }
    return orElse();
  }
}

abstract class NoDataFetched<T> implements Failure<T> {
  const factory NoDataFetched({required String failureMessage}) =
      _$NoDataFetched<T>;

  @override
  String get failureMessage;

  @override
  @JsonKey(ignore: true)
  $NoDataFetchedCopyWith<T, NoDataFetched<T>> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ServerFailureCopyWith<T, $Res>
    implements $FailureCopyWith<T, $Res> {
  factory $ServerFailureCopyWith(
          ServerFailure<T> value, $Res Function(ServerFailure<T>) then) =
      _$ServerFailureCopyWithImpl<T, $Res>;

  @override
  $Res call({String failureMessage});
}

/// @nodoc
class _$ServerFailureCopyWithImpl<T, $Res>
    extends _$FailureCopyWithImpl<T, $Res>
    implements $ServerFailureCopyWith<T, $Res> {
  _$ServerFailureCopyWithImpl(
      ServerFailure<T> _value, $Res Function(ServerFailure<T>) _then)
      : super(_value, (v) => _then(v as ServerFailure<T>));

  @override
  ServerFailure<T> get _value => super._value as ServerFailure<T>;

  @override
  $Res call({
    Object? failureMessage = freezed,
  }) {
    return _then(ServerFailure<T>(
      failureMessage: failureMessage == freezed
          ? _value.failureMessage
          : failureMessage // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$ServerFailure<T> implements ServerFailure<T> {
  const _$ServerFailure({required this.failureMessage});

  @override
  final String failureMessage;

  @override
  String toString() {
    return 'Failure<$T>.serverFailure(failureMessage: $failureMessage)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is ServerFailure<T> &&
            const DeepCollectionEquality()
                .equals(other.failureMessage, failureMessage));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType, const DeepCollectionEquality().hash(failureMessage));

  @JsonKey(ignore: true)
  @override
  $ServerFailureCopyWith<T, ServerFailure<T>> get copyWith =>
      _$ServerFailureCopyWithImpl<T, ServerFailure<T>>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String failureMessage) noDataFetched,
    required TResult Function(String failureMessage) serverFailure,
    required TResult Function(String failureMessage) fieldFailure,
    required TResult Function(String failureMessage) networkFailure,
    required TResult Function(String failureMessage) easyImpactFailure,
    required TResult Function(String failureMessage) mediumImpactFailure,
    required TResult Function(String failureMessage) hardImpactFailure,
  }) {
    return serverFailure(failureMessage);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(String failureMessage)? noDataFetched,
    TResult Function(String failureMessage)? serverFailure,
    TResult Function(String failureMessage)? fieldFailure,
    TResult Function(String failureMessage)? networkFailure,
    TResult Function(String failureMessage)? easyImpactFailure,
    TResult Function(String failureMessage)? mediumImpactFailure,
    TResult Function(String failureMessage)? hardImpactFailure,
  }) {
    return serverFailure?.call(failureMessage);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String failureMessage)? noDataFetched,
    TResult Function(String failureMessage)? serverFailure,
    TResult Function(String failureMessage)? fieldFailure,
    TResult Function(String failureMessage)? networkFailure,
    TResult Function(String failureMessage)? easyImpactFailure,
    TResult Function(String failureMessage)? mediumImpactFailure,
    TResult Function(String failureMessage)? hardImpactFailure,
    required TResult orElse(),
  }) {
    if (serverFailure != null) {
      return serverFailure(failureMessage);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(NoDataFetched<T> value) noDataFetched,
    required TResult Function(ServerFailure<T> value) serverFailure,
    required TResult Function(FieldFailure<T> value) fieldFailure,
    required TResult Function(NetworkFailure<T> value) networkFailure,
    required TResult Function(EasyFailure<T> value) easyImpactFailure,
    required TResult Function(MediumFailure<T> value) mediumImpactFailure,
    required TResult Function(HardFailure<T> value) hardImpactFailure,
  }) {
    return serverFailure(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(NoDataFetched<T> value)? noDataFetched,
    TResult Function(ServerFailure<T> value)? serverFailure,
    TResult Function(FieldFailure<T> value)? fieldFailure,
    TResult Function(NetworkFailure<T> value)? networkFailure,
    TResult Function(EasyFailure<T> value)? easyImpactFailure,
    TResult Function(MediumFailure<T> value)? mediumImpactFailure,
    TResult Function(HardFailure<T> value)? hardImpactFailure,
  }) {
    return serverFailure?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(NoDataFetched<T> value)? noDataFetched,
    TResult Function(ServerFailure<T> value)? serverFailure,
    TResult Function(FieldFailure<T> value)? fieldFailure,
    TResult Function(NetworkFailure<T> value)? networkFailure,
    TResult Function(EasyFailure<T> value)? easyImpactFailure,
    TResult Function(MediumFailure<T> value)? mediumImpactFailure,
    TResult Function(HardFailure<T> value)? hardImpactFailure,
    required TResult orElse(),
  }) {
    if (serverFailure != null) {
      return serverFailure(this);
    }
    return orElse();
  }
}

abstract class ServerFailure<T> implements Failure<T> {
  const factory ServerFailure({required String failureMessage}) =
      _$ServerFailure<T>;

  @override
  String get failureMessage;

  @override
  @JsonKey(ignore: true)
  $ServerFailureCopyWith<T, ServerFailure<T>> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $FieldFailureCopyWith<T, $Res>
    implements $FailureCopyWith<T, $Res> {
  factory $FieldFailureCopyWith(
          FieldFailure<T> value, $Res Function(FieldFailure<T>) then) =
      _$FieldFailureCopyWithImpl<T, $Res>;

  @override
  $Res call({String failureMessage});
}

/// @nodoc
class _$FieldFailureCopyWithImpl<T, $Res> extends _$FailureCopyWithImpl<T, $Res>
    implements $FieldFailureCopyWith<T, $Res> {
  _$FieldFailureCopyWithImpl(
      FieldFailure<T> _value, $Res Function(FieldFailure<T>) _then)
      : super(_value, (v) => _then(v as FieldFailure<T>));

  @override
  FieldFailure<T> get _value => super._value as FieldFailure<T>;

  @override
  $Res call({
    Object? failureMessage = freezed,
  }) {
    return _then(FieldFailure<T>(
      failureMessage: failureMessage == freezed
          ? _value.failureMessage
          : failureMessage // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$FieldFailure<T> implements FieldFailure<T> {
  const _$FieldFailure({required this.failureMessage});

  @override
  final String failureMessage;

  @override
  String toString() {
    return 'Failure<$T>.fieldFailure(failureMessage: $failureMessage)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is FieldFailure<T> &&
            const DeepCollectionEquality()
                .equals(other.failureMessage, failureMessage));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType, const DeepCollectionEquality().hash(failureMessage));

  @JsonKey(ignore: true)
  @override
  $FieldFailureCopyWith<T, FieldFailure<T>> get copyWith =>
      _$FieldFailureCopyWithImpl<T, FieldFailure<T>>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String failureMessage) noDataFetched,
    required TResult Function(String failureMessage) serverFailure,
    required TResult Function(String failureMessage) fieldFailure,
    required TResult Function(String failureMessage) networkFailure,
    required TResult Function(String failureMessage) easyImpactFailure,
    required TResult Function(String failureMessage) mediumImpactFailure,
    required TResult Function(String failureMessage) hardImpactFailure,
  }) {
    return fieldFailure(failureMessage);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(String failureMessage)? noDataFetched,
    TResult Function(String failureMessage)? serverFailure,
    TResult Function(String failureMessage)? fieldFailure,
    TResult Function(String failureMessage)? networkFailure,
    TResult Function(String failureMessage)? easyImpactFailure,
    TResult Function(String failureMessage)? mediumImpactFailure,
    TResult Function(String failureMessage)? hardImpactFailure,
  }) {
    return fieldFailure?.call(failureMessage);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String failureMessage)? noDataFetched,
    TResult Function(String failureMessage)? serverFailure,
    TResult Function(String failureMessage)? fieldFailure,
    TResult Function(String failureMessage)? networkFailure,
    TResult Function(String failureMessage)? easyImpactFailure,
    TResult Function(String failureMessage)? mediumImpactFailure,
    TResult Function(String failureMessage)? hardImpactFailure,
    required TResult orElse(),
  }) {
    if (fieldFailure != null) {
      return fieldFailure(failureMessage);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(NoDataFetched<T> value) noDataFetched,
    required TResult Function(ServerFailure<T> value) serverFailure,
    required TResult Function(FieldFailure<T> value) fieldFailure,
    required TResult Function(NetworkFailure<T> value) networkFailure,
    required TResult Function(EasyFailure<T> value) easyImpactFailure,
    required TResult Function(MediumFailure<T> value) mediumImpactFailure,
    required TResult Function(HardFailure<T> value) hardImpactFailure,
  }) {
    return fieldFailure(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(NoDataFetched<T> value)? noDataFetched,
    TResult Function(ServerFailure<T> value)? serverFailure,
    TResult Function(FieldFailure<T> value)? fieldFailure,
    TResult Function(NetworkFailure<T> value)? networkFailure,
    TResult Function(EasyFailure<T> value)? easyImpactFailure,
    TResult Function(MediumFailure<T> value)? mediumImpactFailure,
    TResult Function(HardFailure<T> value)? hardImpactFailure,
  }) {
    return fieldFailure?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(NoDataFetched<T> value)? noDataFetched,
    TResult Function(ServerFailure<T> value)? serverFailure,
    TResult Function(FieldFailure<T> value)? fieldFailure,
    TResult Function(NetworkFailure<T> value)? networkFailure,
    TResult Function(EasyFailure<T> value)? easyImpactFailure,
    TResult Function(MediumFailure<T> value)? mediumImpactFailure,
    TResult Function(HardFailure<T> value)? hardImpactFailure,
    required TResult orElse(),
  }) {
    if (fieldFailure != null) {
      return fieldFailure(this);
    }
    return orElse();
  }
}

abstract class FieldFailure<T> implements Failure<T> {
  const factory FieldFailure({required String failureMessage}) =
      _$FieldFailure<T>;

  @override
  String get failureMessage;

  @override
  @JsonKey(ignore: true)
  $FieldFailureCopyWith<T, FieldFailure<T>> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $NetworkFailureCopyWith<T, $Res>
    implements $FailureCopyWith<T, $Res> {
  factory $NetworkFailureCopyWith(
          NetworkFailure<T> value, $Res Function(NetworkFailure<T>) then) =
      _$NetworkFailureCopyWithImpl<T, $Res>;

  @override
  $Res call({String failureMessage});
}

/// @nodoc
class _$NetworkFailureCopyWithImpl<T, $Res>
    extends _$FailureCopyWithImpl<T, $Res>
    implements $NetworkFailureCopyWith<T, $Res> {
  _$NetworkFailureCopyWithImpl(
      NetworkFailure<T> _value, $Res Function(NetworkFailure<T>) _then)
      : super(_value, (v) => _then(v as NetworkFailure<T>));

  @override
  NetworkFailure<T> get _value => super._value as NetworkFailure<T>;

  @override
  $Res call({
    Object? failureMessage = freezed,
  }) {
    return _then(NetworkFailure<T>(
      failureMessage: failureMessage == freezed
          ? _value.failureMessage
          : failureMessage // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$NetworkFailure<T> implements NetworkFailure<T> {
  const _$NetworkFailure({required this.failureMessage});

  @override
  final String failureMessage;

  @override
  String toString() {
    return 'Failure<$T>.networkFailure(failureMessage: $failureMessage)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is NetworkFailure<T> &&
            const DeepCollectionEquality()
                .equals(other.failureMessage, failureMessage));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType, const DeepCollectionEquality().hash(failureMessage));

  @JsonKey(ignore: true)
  @override
  $NetworkFailureCopyWith<T, NetworkFailure<T>> get copyWith =>
      _$NetworkFailureCopyWithImpl<T, NetworkFailure<T>>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String failureMessage) noDataFetched,
    required TResult Function(String failureMessage) serverFailure,
    required TResult Function(String failureMessage) fieldFailure,
    required TResult Function(String failureMessage) networkFailure,
    required TResult Function(String failureMessage) easyImpactFailure,
    required TResult Function(String failureMessage) mediumImpactFailure,
    required TResult Function(String failureMessage) hardImpactFailure,
  }) {
    return networkFailure(failureMessage);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(String failureMessage)? noDataFetched,
    TResult Function(String failureMessage)? serverFailure,
    TResult Function(String failureMessage)? fieldFailure,
    TResult Function(String failureMessage)? networkFailure,
    TResult Function(String failureMessage)? easyImpactFailure,
    TResult Function(String failureMessage)? mediumImpactFailure,
    TResult Function(String failureMessage)? hardImpactFailure,
  }) {
    return networkFailure?.call(failureMessage);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String failureMessage)? noDataFetched,
    TResult Function(String failureMessage)? serverFailure,
    TResult Function(String failureMessage)? fieldFailure,
    TResult Function(String failureMessage)? networkFailure,
    TResult Function(String failureMessage)? easyImpactFailure,
    TResult Function(String failureMessage)? mediumImpactFailure,
    TResult Function(String failureMessage)? hardImpactFailure,
    required TResult orElse(),
  }) {
    if (networkFailure != null) {
      return networkFailure(failureMessage);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(NoDataFetched<T> value) noDataFetched,
    required TResult Function(ServerFailure<T> value) serverFailure,
    required TResult Function(FieldFailure<T> value) fieldFailure,
    required TResult Function(NetworkFailure<T> value) networkFailure,
    required TResult Function(EasyFailure<T> value) easyImpactFailure,
    required TResult Function(MediumFailure<T> value) mediumImpactFailure,
    required TResult Function(HardFailure<T> value) hardImpactFailure,
  }) {
    return networkFailure(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(NoDataFetched<T> value)? noDataFetched,
    TResult Function(ServerFailure<T> value)? serverFailure,
    TResult Function(FieldFailure<T> value)? fieldFailure,
    TResult Function(NetworkFailure<T> value)? networkFailure,
    TResult Function(EasyFailure<T> value)? easyImpactFailure,
    TResult Function(MediumFailure<T> value)? mediumImpactFailure,
    TResult Function(HardFailure<T> value)? hardImpactFailure,
  }) {
    return networkFailure?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(NoDataFetched<T> value)? noDataFetched,
    TResult Function(ServerFailure<T> value)? serverFailure,
    TResult Function(FieldFailure<T> value)? fieldFailure,
    TResult Function(NetworkFailure<T> value)? networkFailure,
    TResult Function(EasyFailure<T> value)? easyImpactFailure,
    TResult Function(MediumFailure<T> value)? mediumImpactFailure,
    TResult Function(HardFailure<T> value)? hardImpactFailure,
    required TResult orElse(),
  }) {
    if (networkFailure != null) {
      return networkFailure(this);
    }
    return orElse();
  }
}

abstract class NetworkFailure<T> implements Failure<T> {
  const factory NetworkFailure({required String failureMessage}) =
      _$NetworkFailure<T>;

  @override
  String get failureMessage;

  @override
  @JsonKey(ignore: true)
  $NetworkFailureCopyWith<T, NetworkFailure<T>> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $EasyFailureCopyWith<T, $Res>
    implements $FailureCopyWith<T, $Res> {
  factory $EasyFailureCopyWith(
          EasyFailure<T> value, $Res Function(EasyFailure<T>) then) =
      _$EasyFailureCopyWithImpl<T, $Res>;

  @override
  $Res call({String failureMessage});
}

/// @nodoc
class _$EasyFailureCopyWithImpl<T, $Res> extends _$FailureCopyWithImpl<T, $Res>
    implements $EasyFailureCopyWith<T, $Res> {
  _$EasyFailureCopyWithImpl(
      EasyFailure<T> _value, $Res Function(EasyFailure<T>) _then)
      : super(_value, (v) => _then(v as EasyFailure<T>));

  @override
  EasyFailure<T> get _value => super._value as EasyFailure<T>;

  @override
  $Res call({
    Object? failureMessage = freezed,
  }) {
    return _then(EasyFailure<T>(
      failureMessage: failureMessage == freezed
          ? _value.failureMessage
          : failureMessage // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$EasyFailure<T> implements EasyFailure<T> {
  const _$EasyFailure({required this.failureMessage});

  @override
  final String failureMessage;

  @override
  String toString() {
    return 'Failure<$T>.easyImpactFailure(failureMessage: $failureMessage)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is EasyFailure<T> &&
            const DeepCollectionEquality()
                .equals(other.failureMessage, failureMessage));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType, const DeepCollectionEquality().hash(failureMessage));

  @JsonKey(ignore: true)
  @override
  $EasyFailureCopyWith<T, EasyFailure<T>> get copyWith =>
      _$EasyFailureCopyWithImpl<T, EasyFailure<T>>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String failureMessage) noDataFetched,
    required TResult Function(String failureMessage) serverFailure,
    required TResult Function(String failureMessage) fieldFailure,
    required TResult Function(String failureMessage) networkFailure,
    required TResult Function(String failureMessage) easyImpactFailure,
    required TResult Function(String failureMessage) mediumImpactFailure,
    required TResult Function(String failureMessage) hardImpactFailure,
  }) {
    return easyImpactFailure(failureMessage);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(String failureMessage)? noDataFetched,
    TResult Function(String failureMessage)? serverFailure,
    TResult Function(String failureMessage)? fieldFailure,
    TResult Function(String failureMessage)? networkFailure,
    TResult Function(String failureMessage)? easyImpactFailure,
    TResult Function(String failureMessage)? mediumImpactFailure,
    TResult Function(String failureMessage)? hardImpactFailure,
  }) {
    return easyImpactFailure?.call(failureMessage);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String failureMessage)? noDataFetched,
    TResult Function(String failureMessage)? serverFailure,
    TResult Function(String failureMessage)? fieldFailure,
    TResult Function(String failureMessage)? networkFailure,
    TResult Function(String failureMessage)? easyImpactFailure,
    TResult Function(String failureMessage)? mediumImpactFailure,
    TResult Function(String failureMessage)? hardImpactFailure,
    required TResult orElse(),
  }) {
    if (easyImpactFailure != null) {
      return easyImpactFailure(failureMessage);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(NoDataFetched<T> value) noDataFetched,
    required TResult Function(ServerFailure<T> value) serverFailure,
    required TResult Function(FieldFailure<T> value) fieldFailure,
    required TResult Function(NetworkFailure<T> value) networkFailure,
    required TResult Function(EasyFailure<T> value) easyImpactFailure,
    required TResult Function(MediumFailure<T> value) mediumImpactFailure,
    required TResult Function(HardFailure<T> value) hardImpactFailure,
  }) {
    return easyImpactFailure(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(NoDataFetched<T> value)? noDataFetched,
    TResult Function(ServerFailure<T> value)? serverFailure,
    TResult Function(FieldFailure<T> value)? fieldFailure,
    TResult Function(NetworkFailure<T> value)? networkFailure,
    TResult Function(EasyFailure<T> value)? easyImpactFailure,
    TResult Function(MediumFailure<T> value)? mediumImpactFailure,
    TResult Function(HardFailure<T> value)? hardImpactFailure,
  }) {
    return easyImpactFailure?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(NoDataFetched<T> value)? noDataFetched,
    TResult Function(ServerFailure<T> value)? serverFailure,
    TResult Function(FieldFailure<T> value)? fieldFailure,
    TResult Function(NetworkFailure<T> value)? networkFailure,
    TResult Function(EasyFailure<T> value)? easyImpactFailure,
    TResult Function(MediumFailure<T> value)? mediumImpactFailure,
    TResult Function(HardFailure<T> value)? hardImpactFailure,
    required TResult orElse(),
  }) {
    if (easyImpactFailure != null) {
      return easyImpactFailure(this);
    }
    return orElse();
  }
}

abstract class EasyFailure<T> implements Failure<T> {
  const factory EasyFailure({required String failureMessage}) =
      _$EasyFailure<T>;

  @override
  String get failureMessage;

  @override
  @JsonKey(ignore: true)
  $EasyFailureCopyWith<T, EasyFailure<T>> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $MediumFailureCopyWith<T, $Res>
    implements $FailureCopyWith<T, $Res> {
  factory $MediumFailureCopyWith(
          MediumFailure<T> value, $Res Function(MediumFailure<T>) then) =
      _$MediumFailureCopyWithImpl<T, $Res>;

  @override
  $Res call({String failureMessage});
}

/// @nodoc
class _$MediumFailureCopyWithImpl<T, $Res>
    extends _$FailureCopyWithImpl<T, $Res>
    implements $MediumFailureCopyWith<T, $Res> {
  _$MediumFailureCopyWithImpl(
      MediumFailure<T> _value, $Res Function(MediumFailure<T>) _then)
      : super(_value, (v) => _then(v as MediumFailure<T>));

  @override
  MediumFailure<T> get _value => super._value as MediumFailure<T>;

  @override
  $Res call({
    Object? failureMessage = freezed,
  }) {
    return _then(MediumFailure<T>(
      failureMessage: failureMessage == freezed
          ? _value.failureMessage
          : failureMessage // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$MediumFailure<T> implements MediumFailure<T> {
  const _$MediumFailure({required this.failureMessage});

  @override
  final String failureMessage;

  @override
  String toString() {
    return 'Failure<$T>.mediumImpactFailure(failureMessage: $failureMessage)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is MediumFailure<T> &&
            const DeepCollectionEquality()
                .equals(other.failureMessage, failureMessage));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType, const DeepCollectionEquality().hash(failureMessage));

  @JsonKey(ignore: true)
  @override
  $MediumFailureCopyWith<T, MediumFailure<T>> get copyWith =>
      _$MediumFailureCopyWithImpl<T, MediumFailure<T>>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String failureMessage) noDataFetched,
    required TResult Function(String failureMessage) serverFailure,
    required TResult Function(String failureMessage) fieldFailure,
    required TResult Function(String failureMessage) networkFailure,
    required TResult Function(String failureMessage) easyImpactFailure,
    required TResult Function(String failureMessage) mediumImpactFailure,
    required TResult Function(String failureMessage) hardImpactFailure,
  }) {
    return mediumImpactFailure(failureMessage);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(String failureMessage)? noDataFetched,
    TResult Function(String failureMessage)? serverFailure,
    TResult Function(String failureMessage)? fieldFailure,
    TResult Function(String failureMessage)? networkFailure,
    TResult Function(String failureMessage)? easyImpactFailure,
    TResult Function(String failureMessage)? mediumImpactFailure,
    TResult Function(String failureMessage)? hardImpactFailure,
  }) {
    return mediumImpactFailure?.call(failureMessage);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String failureMessage)? noDataFetched,
    TResult Function(String failureMessage)? serverFailure,
    TResult Function(String failureMessage)? fieldFailure,
    TResult Function(String failureMessage)? networkFailure,
    TResult Function(String failureMessage)? easyImpactFailure,
    TResult Function(String failureMessage)? mediumImpactFailure,
    TResult Function(String failureMessage)? hardImpactFailure,
    required TResult orElse(),
  }) {
    if (mediumImpactFailure != null) {
      return mediumImpactFailure(failureMessage);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(NoDataFetched<T> value) noDataFetched,
    required TResult Function(ServerFailure<T> value) serverFailure,
    required TResult Function(FieldFailure<T> value) fieldFailure,
    required TResult Function(NetworkFailure<T> value) networkFailure,
    required TResult Function(EasyFailure<T> value) easyImpactFailure,
    required TResult Function(MediumFailure<T> value) mediumImpactFailure,
    required TResult Function(HardFailure<T> value) hardImpactFailure,
  }) {
    return mediumImpactFailure(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(NoDataFetched<T> value)? noDataFetched,
    TResult Function(ServerFailure<T> value)? serverFailure,
    TResult Function(FieldFailure<T> value)? fieldFailure,
    TResult Function(NetworkFailure<T> value)? networkFailure,
    TResult Function(EasyFailure<T> value)? easyImpactFailure,
    TResult Function(MediumFailure<T> value)? mediumImpactFailure,
    TResult Function(HardFailure<T> value)? hardImpactFailure,
  }) {
    return mediumImpactFailure?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(NoDataFetched<T> value)? noDataFetched,
    TResult Function(ServerFailure<T> value)? serverFailure,
    TResult Function(FieldFailure<T> value)? fieldFailure,
    TResult Function(NetworkFailure<T> value)? networkFailure,
    TResult Function(EasyFailure<T> value)? easyImpactFailure,
    TResult Function(MediumFailure<T> value)? mediumImpactFailure,
    TResult Function(HardFailure<T> value)? hardImpactFailure,
    required TResult orElse(),
  }) {
    if (mediumImpactFailure != null) {
      return mediumImpactFailure(this);
    }
    return orElse();
  }
}

abstract class MediumFailure<T> implements Failure<T> {
  const factory MediumFailure({required String failureMessage}) =
      _$MediumFailure<T>;

  @override
  String get failureMessage;

  @override
  @JsonKey(ignore: true)
  $MediumFailureCopyWith<T, MediumFailure<T>> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $HardFailureCopyWith<T, $Res>
    implements $FailureCopyWith<T, $Res> {
  factory $HardFailureCopyWith(
          HardFailure<T> value, $Res Function(HardFailure<T>) then) =
      _$HardFailureCopyWithImpl<T, $Res>;

  @override
  $Res call({String failureMessage});
}

/// @nodoc
class _$HardFailureCopyWithImpl<T, $Res> extends _$FailureCopyWithImpl<T, $Res>
    implements $HardFailureCopyWith<T, $Res> {
  _$HardFailureCopyWithImpl(
      HardFailure<T> _value, $Res Function(HardFailure<T>) _then)
      : super(_value, (v) => _then(v as HardFailure<T>));

  @override
  HardFailure<T> get _value => super._value as HardFailure<T>;

  @override
  $Res call({
    Object? failureMessage = freezed,
  }) {
    return _then(HardFailure<T>(
      failureMessage: failureMessage == freezed
          ? _value.failureMessage
          : failureMessage // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$HardFailure<T> implements HardFailure<T> {
  const _$HardFailure({required this.failureMessage});

  @override
  final String failureMessage;

  @override
  String toString() {
    return 'Failure<$T>.hardImpactFailure(failureMessage: $failureMessage)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is HardFailure<T> &&
            const DeepCollectionEquality()
                .equals(other.failureMessage, failureMessage));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType, const DeepCollectionEquality().hash(failureMessage));

  @JsonKey(ignore: true)
  @override
  $HardFailureCopyWith<T, HardFailure<T>> get copyWith =>
      _$HardFailureCopyWithImpl<T, HardFailure<T>>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String failureMessage) noDataFetched,
    required TResult Function(String failureMessage) serverFailure,
    required TResult Function(String failureMessage) fieldFailure,
    required TResult Function(String failureMessage) networkFailure,
    required TResult Function(String failureMessage) easyImpactFailure,
    required TResult Function(String failureMessage) mediumImpactFailure,
    required TResult Function(String failureMessage) hardImpactFailure,
  }) {
    return hardImpactFailure(failureMessage);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(String failureMessage)? noDataFetched,
    TResult Function(String failureMessage)? serverFailure,
    TResult Function(String failureMessage)? fieldFailure,
    TResult Function(String failureMessage)? networkFailure,
    TResult Function(String failureMessage)? easyImpactFailure,
    TResult Function(String failureMessage)? mediumImpactFailure,
    TResult Function(String failureMessage)? hardImpactFailure,
  }) {
    return hardImpactFailure?.call(failureMessage);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String failureMessage)? noDataFetched,
    TResult Function(String failureMessage)? serverFailure,
    TResult Function(String failureMessage)? fieldFailure,
    TResult Function(String failureMessage)? networkFailure,
    TResult Function(String failureMessage)? easyImpactFailure,
    TResult Function(String failureMessage)? mediumImpactFailure,
    TResult Function(String failureMessage)? hardImpactFailure,
    required TResult orElse(),
  }) {
    if (hardImpactFailure != null) {
      return hardImpactFailure(failureMessage);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(NoDataFetched<T> value) noDataFetched,
    required TResult Function(ServerFailure<T> value) serverFailure,
    required TResult Function(FieldFailure<T> value) fieldFailure,
    required TResult Function(NetworkFailure<T> value) networkFailure,
    required TResult Function(EasyFailure<T> value) easyImpactFailure,
    required TResult Function(MediumFailure<T> value) mediumImpactFailure,
    required TResult Function(HardFailure<T> value) hardImpactFailure,
  }) {
    return hardImpactFailure(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(NoDataFetched<T> value)? noDataFetched,
    TResult Function(ServerFailure<T> value)? serverFailure,
    TResult Function(FieldFailure<T> value)? fieldFailure,
    TResult Function(NetworkFailure<T> value)? networkFailure,
    TResult Function(EasyFailure<T> value)? easyImpactFailure,
    TResult Function(MediumFailure<T> value)? mediumImpactFailure,
    TResult Function(HardFailure<T> value)? hardImpactFailure,
  }) {
    return hardImpactFailure?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(NoDataFetched<T> value)? noDataFetched,
    TResult Function(ServerFailure<T> value)? serverFailure,
    TResult Function(FieldFailure<T> value)? fieldFailure,
    TResult Function(NetworkFailure<T> value)? networkFailure,
    TResult Function(EasyFailure<T> value)? easyImpactFailure,
    TResult Function(MediumFailure<T> value)? mediumImpactFailure,
    TResult Function(HardFailure<T> value)? hardImpactFailure,
    required TResult orElse(),
  }) {
    if (hardImpactFailure != null) {
      return hardImpactFailure(this);
    }
    return orElse();
  }
}

abstract class HardFailure<T> implements Failure<T> {
  const factory HardFailure({required String failureMessage}) =
      _$HardFailure<T>;

  @override
  String get failureMessage;

  @override
  @JsonKey(ignore: true)
  $HardFailureCopyWith<T, HardFailure<T>> get copyWith =>
      throw _privateConstructorUsedError;
}
