import 'package:dartz/dartz.dart';

import '../../core/errors/failures.dart';

abstract class VideoRecorderRepository {
  Future<Either<Failure, String>> uploadVideo(String path, String name);
}
