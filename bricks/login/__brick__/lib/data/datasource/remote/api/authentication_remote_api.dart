import 'dart:convert';

import 'package:aad_oauth/aad_oauth.dart';
import 'package:aad_oauth/model/config.dart';
import 'package:dio/dio.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:injectable/injectable.dart';
import 'package:jwt_decode/jwt_decode.dart';
import '../../../../core/utils/extensions.dart';

import '../../../../core/errors/exceptions.dart';
import '../../../../core/utils/constants.dart';
import '../components/custom_dio.dart';

@singleton
class AuthenticationRemoteApi {
  CustomDio dio = CustomDio();

  Future<Response> login(
    String email,
    String password, {
    String? microsoftToken,
  }) async {
    await dotenv.load();
    final String loginUrl =
        "${dotenv.env[AppConst.IDENTITY_URL] ?? ''}authentication/login";
    final String dataJson = json.encode({
      'username': email,
      'password': password,
      'adDomainId': 0,
      'token': microsoftToken ?? 'string'
    });
    final response = await dio.makeSecureCall(
      () => dio.instance.postUri(Uri.parse(loginUrl), data: dataJson),
    );

    return response.fold(
      (failure) => throw HardException(runtimeType, failure.failureMessage),
      (response) {
        if (response.statusCode == 200) {
          return response;
        }
        if (response.statusCode == 500) {
          throw MediumException(
            runtimeType,
            AppString.loginFailedError.translate(),
          );
        }
        if (response.statusCode == 401) {
          if ((response.data['messages'] as List<dynamic>)
                  .first['translationCode'] ==
              'Authentication.Login.UnauthorizedAccess') {
            throw MediumException(
              runtimeType,
              AppString.loginFailedError.translate(),
            );
          }
          throw MediumException(
            runtimeType,
            AppString.accessDenied.translate(),
          );
        }
        throw HardException(
          runtimeType,
          AppString.unexpectedFailure.translate(),
        );
      },
    );
  }

  Future<Response> logInWithMicrosoft() async {
    await dotenv.load();
    final config = Config(
      tenant: dotenv.env[AppConst.MSAL_TENANT]!,
      clientId: dotenv.env[AppConst.MSAL_CLIENT_ID]!,
      scope: dotenv.env[AppConst.MSAL_SCOPE]!,
      redirectUri: dotenv.env[AppConst.MSAL_REDIRECT_URI]!,
    );
    final oauth = AadOAuth(config);

    await oauth.logout();
    await oauth.login();

    final accessToken = await oauth.getIdToken();
    final tokenDecoded = Jwt.parseJwt(accessToken!);
    final preferredUsername = tokenDecoded['preferred_username'] as String;
    return login(preferredUsername, '', microsoftToken: accessToken);
  }

  Future<Response> changePassword(
    String currentPassword,
    String newPassword,
  ) async {
    await dotenv.load();
    final String changePasswordUrl =
        "${dotenv.env[AppConst.IDENTITY_URL] ?? ''}authentication/change-password";

    final response = await dio.makeSecureCall(
      () => dio.instance.putUri(
        Uri.parse(changePasswordUrl),
        data: {
          'currentPassword': currentPassword,
          'newPassword': newPassword,
        },
      ),
    );

    return response.fold(
      (failure) => throw HardException(runtimeType, failure.failureMessage),
      (response) {
        if (response.statusCode == 200) {
          return response;
        }
        if (response.statusCode == 500) {
          throw MediumException(
            runtimeType,
            AppString.changePasswordFailedError.translate(),
          );
        }
        if (response.statusCode == 401) {
          if ((response.data['messages'] as List<dynamic>)
                  .first['translationCode'] ==
              'Authentication.ChangePassword.WrongPassword') {
            throw MediumException(
              runtimeType,
              AppString.changePasswordWrong.translate(),
            );
          }
          throw MediumException(
            runtimeType,
            AppString.accessDenied.translate(),
          );
        }
        throw HardException(
          runtimeType,
          AppString.unexpectedFailure.translate(),
        );
      },
    );
  }
}
