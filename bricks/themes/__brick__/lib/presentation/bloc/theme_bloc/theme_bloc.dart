import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:meta/meta.dart';
import '../../../core/utils/helpers.dart';
import '../../../core/utils/constants.dart';
part 'theme_event.dart';
part 'theme_state.dart';

enum AppTheme {
  Dark,
  Light,
}

final appThemeData = {
  AppTheme.Dark: ThemeData(
    colorScheme: ColorScheme.fromSeed(
      brightness: Brightness.dark,
      seedColor: Colors.orange,
      primary: Colors.orange[800],
      secondary: Colors.blue,
      secondaryContainer: Colors.white,
      onSecondaryContainer: Colors.black,
      background: hexToColor(AppColors.darkBlue),
    ),
    useMaterial3: true,
  ),
  AppTheme.Light: ThemeData(
    colorScheme: ColorScheme.fromSeed(
      seedColor: Colors.orange,
      primary: Colors.orange[800],
      secondary: Colors.blue,
      secondaryContainer: Colors.white,
      onSecondaryContainer: Colors.black,
      background: hexToColor(AppColors.darkBlue),
    ),
    useMaterial3: true,
  ),
};

class ThemeBloc extends Bloc<ThemeEvent, ThemeState> {

  ThemeBloc():super(ThemeState(themeData: appThemeData[AppTheme.{{theme_type}}]!)){
    on<ThemeChanged>((event, emit) {
      emit(ThemeState(themeData: appThemeData[event.theme]!));
    });
  }
}
