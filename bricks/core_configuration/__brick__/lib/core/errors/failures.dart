import 'package:freezed_annotation/freezed_annotation.dart';

part 'failures.freezed.dart';

@freezed
class Failure<T> with _$Failure<T> {
  const factory Failure.noDataFetched({required String failureMessage}) =
  NoDataFetched<T>;

  const factory Failure.serverFailure({required String failureMessage}) =
  ServerFailure<T>;

  const factory Failure.fieldFailure({required String failureMessage}) =
  FieldFailure<T>;

  const factory Failure.networkFailure({required String failureMessage}) =
  NetworkFailure<T>;

  const factory Failure.easyImpactFailure({required String failureMessage}) =
  EasyFailure<T>;

  const factory Failure.mediumImpactFailure({required String failureMessage}) =
  MediumFailure<T>;

  const factory Failure.hardImpactFailure({required String failureMessage}) =
  HardFailure<T>;
}