# Material 3

***Material documentation:***  [https://m3.material.io/styles/color/the-color-system/key-colors-tones](https://m3.material.io/styles/color/the-color-system/key-colors-tones)

### Code utils

```jsx
//to access the current theme of the app with it's color scheme
Theme.of(context).colorScheme,

//theme example
ThemeData(
    colorScheme: ColorScheme.fromSeed(
      brightness: Brightness.dark,
      seedColor: Colors.orange,
      primary: Colors.orange[800],
      secondary: Colors.blue,
      secondaryContainer: Colors.white,
      onSecondaryContainer: Colors.black,
      background: hexToColor(AppColors.darkBlue),
    ),
    useMaterial3: true,
  )
```


### Base colors

🟢 The **primary** key color is used to derive roles for key components across the UI, such as the FAB, prominent buttons, active states, as well as the tint of elevated surfaces.  

🟣 The **secondary** key color is used for less prominent components in the UI such as filter chips, while expanding the opportunity for color expression.  

🟡 The **tertiary** key color is used to derive the roles of contrasting accents that can be used to balance primary and secondary colors or bring heightened attention to an element. The tertiary color role is left for teams to use at their discretion and is intended to support broader color expression in products.

![image1.png](../../assets/image1.png)

### Others

🟤 The **neutral** key color is used to derive the roles of surface and background, as well as high emphasis text and icons.

🟠 The **neutral variant** key color is used to derive medium emphasis text and icons, surface variants, and component outlines.

🔴 The **error** key color is used to handle the error colors.

![image2.png](../../assets/image2.png)

### **Tonal palettes**

> A ***tonal palette*** consists of thirteen tones, including white and black. A tone value of 100 is equivalent to the idea of light at its maximum and results in white. Every tone value between 0 and 100 expresses the amount of light present in the color.
>

![image3.png](../../assets/image3.png)

The 100 tone is always 100% white, the lightest tone in the range; the 0 tone is 100% black, the darkest tone in the range

### Useful links

- [Roles](https://m3.material.io/styles/color/the-color-system/color-roles)

- [Tokens](https://m3.material.io/styles/color/the-color-system/tokens)

- [Custom colors](https://m3.material.io/styles/color/the-color-system/custom-colors)

- [Accessibility](https://m3.material.io/styles/color/the-color-system/accessibility)