import 'dart:io';

import 'package:dk_shift_in/presentation/pages/vide_recorder_page/page/video_recorder.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:sizer/sizer.dart';
import 'presentation/bloc/theme_bloc/theme_bloc.dart';
import 'core/di/injectable.dart';
import '/presentation/pages/splash_page/splash_page.dart';
import 'presentation/bloc/authentication_bloc/authentication_bloc.dart';
import '/presentation/pages/login/login_page.dart';
import 'presentation/bloc/localisation_bloc/localisation_bloc.dart';
import 'package:camera/camera.dart';

List<CameraDescription> _cameras = <CameraDescription>[];

Future<void> main() async {
  HttpOverrides.global = MyHttpOverrides();
  _cameras = await availableCameras();
  configureDependencies();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider<AuthenticationBloc>(
          create: (BuildContext context) =>
              getIt<AuthenticationBloc>()..add(LoginFetchDataEvent()),
        ),
        BlocProvider<ThemeBloc>(
          create: (BuildContext context) => getIt<ThemeBloc>(),
        ),
        BlocProvider<LocalisationBloc>(
          create: (BuildContext context) =>
              getIt<LocalisationBloc>()..add(LocalisationLanguageForLogin()),
        ),
      ],
      child: BlocBuilder<LocalisationBloc, LocalisationState>(
        builder: (context, state) {
          return Sizer(
            builder: (context, orientation, deviceType) {
              return BlocBuilder<ThemeBloc, ThemeState>(
                builder: (context, state) {
                  return MaterialApp(
                    theme: state.themeData,
                    builder: EasyLoading.init(),
                    home: VideRecordingPage(
                      cameras: _cameras,
                    ),
                  );
                },
              );
            },
          );
        },
      ),
    );
  }
}

class MyHttpOverrides extends HttpOverrides {
  @override
  HttpClient createHttpClient(SecurityContext? context) {
    return super.createHttpClient(context)
      ..badCertificateCallback =
          (X509Certificate cert, String host, int port) => true;
  }
}
