import 'dart:convert';

import '../../../models/{{pageName.snakeCase()}}/user_info_model.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import '../../../../core/utils/extensions.dart';

import '../../../../core/errors/exceptions.dart';
import '../../../../core/utils/constants.dart';
import '../components/custom_dio.dart';

class {{pageName.pascalCase()}}RemoteApi {
  CustomDio dio = CustomDio();

  Future<UserInfoModel> getUserInfo() async {
    await dotenv.load();
    final String url =
        "${dotenv.env[AppConst.IDENTITY_URL] ?? ''}authentication/login";

    final response = await dio.makeSecureCall(
      () => dio.instance.getUri(
        Uri.parse('https://jsonplaceholder.typicode.com/todos/1'),
      ),
    );

    return response.fold(
        (failure) => throw HardException(runtimeType, failure.failureMessage),
        (response) {
      if (response.statusCode == 200) {
        final model = UserInfoModel.fromJson(
          response.data as Map<String, dynamic>,
        );
        return model;
      } else {
        throw MediumException(
          runtimeType,
          AppString.wrongStatusCode.translate(),
        );
      }
    });
  }

  Future<String> completeTask(bool taskStatus) async {
    await dotenv.load();
    final String url =
        "${dotenv.env[AppConst.IDENTITY_URL] ?? ''}authentication/login";
    final String dataJson = json.encode({
      'id': 1,
      'description': 'update from post',
      'isTaskCompleted': taskStatus,
      'userId': 1
    });

    //actually this is invalid url, the post is ok, but the api url is a wrong one.
    final response = await dio.makeSecureCall(
      () => dio.instance.postUri(
        Uri.parse('https://jsonplaceholder.typicode.com/todos/1'),
        data: dataJson,
      ),
    );
    return response.fold(
        (failure) => throw HardException(runtimeType, failure.failureMessage),
        (response) {
      if (response.statusCode == 200) {
        return 'Good Job';
      } else {
        throw MediumException(
          runtimeType,
          AppString.wrongStatusCode.translate(),
        );
      }
    });
  }
}
