// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// InjectableConfigGenerator
// **************************************************************************

import 'package:get_it/get_it.dart' as _i1;
import 'package:injectable/injectable.dart' as _i2;

{{#login_available}}import '../../domain/validators/authentication_validator.dart' as _i7;
import '../../presentation/bloc/authentication_bloc/authentication_bloc.dart'
as _i37;
import '../../domain/usecases/authentication_usecase.dart' as _i27;
import '../../domain/repositories_contracts/authentication_repo.dart' as _i5;
import '../../data/repositories_imp/authentication_repo_impl.dart' as _i6;{{/login_available}}

{{#localisation_available}}
import '../utils/translator.dart' as _i26;
import '../../data/datasource/remote/api/localisation_remote_api.dart' as _i17;
import '../../data/repositories_imp/localisation_repo_impl.dart' as _i34;
import '../../presentation/bloc/localisation_bloc/localisation_bloc.dart'
as _i39;
import '../../domain/repositories_contracts/localisation_repo.dart' as _i33;
{{/localisation_available}}
{{#theme_available}}import '../../presentation/bloc/theme_bloc/theme_bloc.dart' as _i40;  {{/theme_available}}

/// initializes the registration of provided dependencies inside of [GetIt]
_i1.GetIt $initGetIt(_i1.GetIt get,
    {String? environment, _i2.EnvironmentFilter? environmentFilter}) {
  final gh = _i2.GetItHelper(get, environment, environmentFilter);

  {{#theme_available}}
  gh.factory<_i40.ThemeBloc>(() => _i40.ThemeBloc());
  {{/theme_available}}

  {{#login_available}} gh.factory<_i27.AuthenticationUseCase>(() => _i27.AuthenticationUseCase(
      authRepo: get<_i5.AuthenticationRepo>(),
      fieldValidator: get<_i7.AuthenticationValidator>()));
  gh.singleton<_i5.AuthenticationRepo>(_i6.AuthRepoImpl());
  gh.factory<_i7.AuthenticationValidator>(() => _i7.AuthenticationValidator());
  gh.singleton<_i37.AuthenticationBloc>(_i37.AuthenticationBloc(
      authenticationUseCase: get<_i27.AuthenticationUseCase>()));{{/login_available}}

  {{#localisation_available}}
  gh.singleton<_i17.LocalisationRemoteApi>(_i17.LocalisationRemoteApi());
  gh.singleton<_i26.Translator>(_i26.Translator());

  gh.singleton<_i33.LocalisationRepository>(_i34.LocalisationRepoImpl(
  localisationRemoteApi: get<_i17.LocalisationRemoteApi>(),
  translator: get<_i26.Translator>()));

  gh.factory<_i39.LocalisationBloc>(() => _i39.LocalisationBloc(
  localisationRepo: get<_i33.LocalisationRepository>()));
  {{/localisation_available}}
  return get;
}
