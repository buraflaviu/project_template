import 'package:dartz/dartz.dart';

import '../../core/errors/failures.dart';

abstract class AuthenticationRepo {
  Future<Either<Failure, String>> loginClassic(
    String email,
    String password,
    bool isRememberMeAccepted,
  );

  Future<Either<Failure, String>> loginWithMicrosoft();

  Future<Either<Failure, String>> changePassword(
      String currentPassword,
      String newPassword,
      );
}
