import 'package:flutter/material.dart';
import 'package:mobile_scanner/mobile_scanner.dart';

// FOR IOS in Info.plist
// <key>io.flutter.embedded_views_preview</key>
// <true/>
// <key>NSCameraUsageDescription</key>
// <string>This app needs camera access to scan QR codes</string>
//
// In podfile:
// platform :ios, '11.0'

class ScannerPage extends StatefulWidget {
  const ScannerPage({Key? key}) : super(key: key);

  @override
  State<ScannerPage> createState() => _ScannerPageState();
}

class _ScannerPageState extends State<ScannerPage> {
  String scannedValue = 'nothing detected';
  bool scanInProgress = true;
  MobileScannerController controller = MobileScannerController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SizedBox(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            SizedBox(
              width: 400,
              height: 400,
              child: MobileScanner(
                allowDuplicates: true,
                fit: BoxFit.contain,
                controller: controller,
                onDetect: (barcode, args) {
                  setState(() {
                    scannedValue = barcode.rawValue ?? 'nothing detected';
                  });
                },
              ),
            ),
            ElevatedButton(
              onPressed: () {
                setState(() {
                  scanInProgress ? controller.stop() : controller.start();
                  scanInProgress = !scanInProgress;
                });
              },
              child: Text(scanInProgress ? 'Stop scan' : 'Start scan'),
            ),
            Text(scannedValue),
            ElevatedButton(
              onPressed: () {
                setState(() {
                  scannedValue = 'nothing detected';
                });
              },
              child: const Text('Clean last scanned value'),
            ),
          ],
        ),
      ),
    );
  }
}
