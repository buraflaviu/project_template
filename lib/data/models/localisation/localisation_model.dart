import 'translated_word_model.dart';

class LocalisationModel {
  late int id;
  late String code;
  late String name;
  late List<TranslatedWord>? listOfTranslatedWords;

  LocalisationModel({
    required this.id,
    required this.code,
    required this.name,
    this.listOfTranslatedWords,
  });

  LocalisationModel.fromJson(Map<String, dynamic> json) {
    id = json['id'] as int;
    code = json['code'] as String;
    name = json['name'] as String;
    if (json['translations'] != null) {
      listOfTranslatedWords = <TranslatedWord>[];
      json['translations'].forEach((v) {
        listOfTranslatedWords!
            .add(TranslatedWord.fromJson(v as Map<String, dynamic>));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['code'] = code;
    data['name'] = name;
    if (listOfTranslatedWords != null) {
      data['translations'] =
          listOfTranslatedWords!.map((v) => v.toJson()).toList();
    }
    return data;
  }

  Map<String, String> getTranslation() {
    final Map<String, String> data = {};
    if (listOfTranslatedWords != null) {
      for (final word in listOfTranslatedWords!) {
        data['${word.translationCode}'] = '${word.translation}';
      }
    }
    return data;
  }
}
