import 'package:dartz/dartz.dart';

import '../../core/errors/exceptions.dart';
import '../../core/errors/failures.dart';
import '../../domain/entities/user_info_entity.dart';

import '../../domain/repositories_contracts/{{pageName.snakeCase()}}_repo.dart';
import '../datasource/remote/api/{{pageName.snakeCase()}}_remote_api.dart';

class {{pageName.pascalCase()}}RepositoryImpl extends {{pageName.pascalCase()}}Repository {
{{pageName.pascalCase()}}RemoteApi remoteApi = {{pageName.pascalCase()}}RemoteApi();

  @override
  Future<Either<Failure, UserInfoEntity>> getUserInfo() async {
    try {
      final model = await remoteApi.getUserInfo();
      return Right(model);
    } on AppException catch (appException) {
      return Left(HardFailure(failureMessage: appException.message));
    } catch (exception) {
      return Left(ServerFailure(failureMessage: exception.toString()));
    }
  }

  @override
  Future<Either<Failure, String>> completeTask(bool isTaskToBeCompleted) async {
    try {
      final message = await remoteApi.completeTask(isTaskToBeCompleted);
      return Right(message);
    } on AppException catch (appException) {
      return Left(HardFailure(failureMessage: appException.message));
    } catch (exception) {
      return Left(ServerFailure(failureMessage: exception.toString()));
    }
  }
}
