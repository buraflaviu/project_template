class LanguageModel {
  final int id;
  final String isoCode;
  final String name;

  const LanguageModel({
    required this.id,
    required this.isoCode,
    required this.name,
  });

  factory LanguageModel.fromJson(Map<String, dynamic> json) {
    return LanguageModel(
      id: json['id'] as int,
      isoCode: json['isoCode'] as String,
      name: json['name'] as String,
    );
  }

  Map<String, dynamic> toJson() => {
        'id': id,
        'isoCode': isoCode,
        'name': name,
      };

  LanguageModel copyWith({int? id, String? isoCode, String? name}) =>
      LanguageModel(
        id: id ?? this.id,
        isoCode: isoCode ?? this.isoCode,
        name: name ?? this.name,
      );
}
