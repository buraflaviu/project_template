import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:integration_test/integration_test.dart';
import 'package:sizer/sizer.dart';

import '../lib/core/di/injectable.dart';
import '../lib/core/utils/constants.dart';
import '../lib/data/repositories_imp/authentication_repo_impl.dart';
import '../lib/domain/usecases/authentication_usecase.dart';
import '../lib/domain/validators/authentication_validator.dart';
import '../lib/presentation/bloc/authentication_bloc/authentication_bloc.dart';
import '../lib/presentation/bloc/localisation_bloc/localisation_bloc.dart';
import '../lib/presentation/pages/login/login_page.dart';
import '../lib/presentation/shared_widgets/credentials_field.dart';
import '../lib/presentation/shared_widgets/elevated_action_button.dart';

const String wrongString = 'wrong_string';
const String correctUsername = 'testuser1.svdesk';
const String correctPassword = '12345678A';

void main() {
  IntegrationTestWidgetsFlutterBinding.ensureInitialized();
  String? storedUsername, storedPassword;

  setUpAll(() async {
    configureDependencies();
    storedUsername = await FlutterSecureStorage().read(key: AppKey.USER_EMAIL);
    storedPassword =
        await FlutterSecureStorage().read(key: AppKey.USER_PASSWORD);
  });

  Widget loginPage() => MultiBlocProvider(
        providers: [
          BlocProvider<AuthenticationBloc>(
            create: (BuildContext context) => AuthenticationBloc(
              authenticationUseCase: AuthenticationUseCase(
                authRepo: AuthRepoImpl(),
                fieldValidator: AuthenticationValidator(),
              ),
            )..add(LoginFetchDataEvent()),
          ),
          BlocProvider<LocalisationBloc>(
            create: (BuildContext context) =>
                getIt<LocalisationBloc>()..add(LocalisationLanguageForLogin()),
          ),
        ],
        child: Sizer(
          builder: (context, orientation, deviceType) =>
              BlocBuilder<LocalisationBloc, LocalisationState>(
            builder: (context, state) =>
                MaterialApp(home: LoginPage(), builder: EasyLoading.init()),
          ),
        ),
      );

  group('Authentication tests', () {
    final usernameField =
        find.widgetWithText(CredentialsField, AppString.fieldEmailHint);
    final passwordField =
        find.widgetWithText(CredentialsField, AppString.fieldPasswordHint);
    final loginButton =
        find.widgetWithText(ElevatedActionButton, AppString.logIn);
    final termsAndPolicy = find.byType(Checkbox).last;

    testWidgets('Test if the stored values are displayed, expect SUCCESS',
        (WidgetTester tester) async {
      await tester.pumpWidget(loginPage());
      await tester.pumpAndSettle();
      if (storedUsername != null && storedPassword != null) {
        expect(find.text(storedUsername!), findsOneWidget);
        expect(find.text(storedPassword!), findsOneWidget);
      } else {
        expect(find.text(AppString.fieldEmailHint), findsOneWidget);
        expect(find.text(AppString.fieldPasswordHint), findsOneWidget);
      }
    });

    testWidgets('Test if no credentials are entered, expect ERROR',
        (WidgetTester tester) async {
      await tester.pumpWidget(loginPage());
      await tester.pumpAndSettle();
      await tester.enterText(usernameField, AppConst.EMPTY_STRING);
      await tester.enterText(passwordField, AppConst.EMPTY_STRING);

      await tester.tap(loginButton);
      await tester.pumpAndSettle();

      expect(find.text(AppString.credentialsNotValidError), findsOneWidget);
    });

    testWidgets('Test wrong credentials with terms checked, expect ERROR',
        (WidgetTester tester) async {
      await tester.pumpWidget(loginPage());
      await tester.pumpAndSettle();
      await tester.enterText(usernameField, wrongString);
      await tester.enterText(passwordField, wrongString);

      await tester.tap(termsAndPolicy);
      await tester.tap(loginButton);
      await tester.pumpAndSettle();

      expect(find.text(AppString.termsNotCheckedError), findsNothing);
      expect(find.text(AppString.loginFailedError), findsOneWidget);
    });

    testWidgets('Test random credentials without terms checked, expect ERROR',
        (WidgetTester tester) async {
      await tester.pumpWidget(loginPage());
      await tester.pumpAndSettle();

      await tester.enterText(usernameField, wrongString);
      await tester.enterText(passwordField, wrongString);

      await tester.tap(loginButton);
      await tester.pumpAndSettle();

      expect(find.text(AppString.termsNotCheckedError), findsOneWidget);
    });

    testWidgets('Test correct username and wrong password, expect ERROR',
        (WidgetTester tester) async {
      await tester.pumpWidget(loginPage());
      await tester.pumpAndSettle();
      await tester.enterText(usernameField, wrongString);
      await tester.enterText(passwordField, correctPassword);

      await tester.tap(termsAndPolicy);
      await tester.tap(loginButton);
      await tester.pumpAndSettle();

      expect(find.text(AppString.termsNotCheckedError), findsNothing);
      expect(find.text(AppString.loginFailedError), findsOneWidget);
    });

    testWidgets('Test wrong username and correct password, expect ERROR',
        (WidgetTester tester) async {
      await tester.pumpWidget(loginPage());
      await tester.pumpAndSettle();
      await tester.enterText(usernameField, correctUsername);
      await tester.enterText(passwordField, wrongString);

      await tester.tap(termsAndPolicy);
      await tester.tap(loginButton);
      await tester.pumpAndSettle();

      expect(find.text(AppString.termsNotCheckedError), findsNothing);
      expect(find.text(AppString.loginFailedError), findsOneWidget);
    });

    testWidgets('Test correct credentials entered',
        (WidgetTester tester) async {
      await tester.pumpWidget(loginPage());
      await tester.pumpAndSettle();

      await tester.enterText(usernameField, correctUsername);
      await tester.enterText(passwordField, correctPassword);

      await tester.tap(termsAndPolicy);

      await tester.tap(loginButton);
      await tester.pumpAndSettle();

      expect(find.text(AppString.termsNotCheckedError), findsNothing);
      expect(find.text(AppString.loginFailedError), findsNothing);
    });
  });
}
